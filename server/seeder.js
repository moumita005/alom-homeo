const fs = require('fs');
const colors = require('colors');
const dotenv = require('dotenv');

//Load env vars
dotenv.config({ path: './config/config.env' });


//Load models
const Patient = require('./models/Patient');


//Connect to database
