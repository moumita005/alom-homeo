const { Sequelize } = require('sequelize');


const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST, 
    dialect: 'mysql',
    // socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock',
    define: {
      underscored: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    },

   
  }
);

const connectSQL = () => {
  sequelize.authenticate()
  .then(() => console.log('SQL Database connected...'.cyan.underline.bold))
  .catch(err => console.log('ERROR: ' + err));
};


module.exports = {
  sequelize,
  Sequelize,
  connectSQL
};
