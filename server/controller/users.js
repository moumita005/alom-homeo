const asyncHandler = require('../middleware/async');
const {sequelize} = require('../config/db')
const User = require('../models/User');
const ErrorResponse = require('../utlis/errorResponse');


exports.getUsers = asyncHandler(async(req,res,next) => {
  const users = await User.findAll();

  res.status(200).send({
    success:true,
    count: users.length,
    message: "get all users",
    data: users
  })
})



exports.getUser = asyncHandler(async(req,res,next) => {
  const user = await User.findByPk(req.params.id);

  if(!user){
    return next(new ErrorResponse(`no user with this id ${req.params.id}`))
  }

  res.status(200).send({
    success:true,
    message:`show the user with the id ${req.params.id}`,
    data:user
  })
})

exports.createUser = asyncHandler(async(req,res,next) =>{
  let user = await User.findOne({ where: { email: req.body.email } })
  // console.log(student)

  if(user) {
    return next(new ErrorResponse('email id already exists', 400))
  }
  user = await User.create(req.body);
  
  res.status(201).send({
    success:true,
    data: user
  })
  
})


exports.updateUser = asyncHandler(async(req,res,next) => {
  const user = await User.update(req.body,{
    where:{_id: req.params.id},
    returning:true
  })
  
  const newUser = await User.findByPk(req.params.id)
  if(!newUser){
    return next(new ErrorResponse(`no user with this id ${req.params.id}`))
  }

  res.status(200).send({
    success:true,
    data: newUser
  })
})


exports.deleteUser = asyncHandler(async(req,res,next) => {
  const user = await User.findByPk(req.params.id);

  if(!user){
    return next(new ErrorResponse(`no user with this id ${req.params.id}`))
  }

  await user.destroy(req.params.id);

  res.status(200).send({
    success: true,
    data: {}
  })


})