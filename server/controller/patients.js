const ErrorResponse = require('../utlis/errorResponse')
const { Patient, Prescription } = require('../models');
const asyncHandler = require('../middleware/async');


//@desc            get all the patients
//@route           GET /api/patients
//@access          Public
exports.getPatients = asyncHandler(async(req,res,next) => {

  const patients = await Patient.findAll({
    include: [Prescription]
  });
  res.status(200).send({
    success: true,
    msg: `show all patinets ${patients.length}`,
    data: patients
  })
  
  
})


//@desc            get single the patients
//@route           GET /api/patients/:id
//@access          Public
exports.getPatient = asyncHandler(async(req,res,next) => {

    const patient = await Patient.findByPk(req.params.id);
    if(!patient){
      return  next(new ErrorResponse(`Patient not found with id of ${req.params.id}`,404))
      // res.status(404).send({
      //   success: false,
      //   msg: `can't find patinet ${req.params.id}`,
      // })
    }
    res.status(200).send({
      success: true,
      msg: `show patinet ${req.params.id}`,
      data: patient
    })
  
})

//@desc            create new patients
//@route           POST /api/patients
//@access          Private
exports.createPatient =asyncHandler( async(req,res,next) => {

    const patient = await Patient.create(req.body)
    res.status(201).send({
      success: true,
      msg: 'create  patinet',
      data: patient
    })

  
})


//@desc            update patients
//@route           PUT /api/patients/:id
//@access          Private
exports.updatePatient = asyncHandler(async(req,res,next) => {

    let [patient, isUpdated] = await Patient.update(req.body, {
      where: { id: req.params.id },
      returning: true,
      // plain:true
    })
    console.log(req.body)
    let newPatient = await Patient.findByPk(req.params.id);
    console.log(newPatient)

    if(!isUpdated){
      return next(new ErrorResponse(`Patient with ${req.params.id} can not updated`,404))
    }

    

    res.status(200).send({
      success:true, 
      message: `update patinet ${req.params.id}`,
      data: newPatient
    })
    
  
})


//@desc            delete patients
//@route           DELETE /api/patients/:id
//@access          Private
exports.deletePatient = asyncHandler( async(req,res,next) => {

    const patient = await Patient.findByPk(req.params.id);
    if(!patient){
      return next(new ErrorResponse(`Patient with ${req.params.id} not found`,404))
    }
    await patient.destroy(req.params.id);
    res.status(200).send({
      success: true, 
      message: `delete patient ${req.params.id}`, 
      data: {} 
    })
  
  
})
