const asyncHandler = require('../middleware/async');
const { Prescription, Medicine } = require('../models');
const ErrorResponse = require('../utlis/errorResponse');



exports.getMedicines = asyncHandler(async(req,res,next) =>{

  const medicines = await Medicine.findAll({
    include: [Prescription]
  });

  res.status(200).json({
    success: true,
    message: `show all medicines ${medicines.length}`,
    data: medicines
  })
})


exports.getMedicine = asyncHandler(async(req,res,next) => {

  const medicine = await Medicine.findByPk(req.params.id);

  if(!medicine) return next(new ErrorResponse(`Medicine with ${req.params.id} not found`,404));
  
  res.status(200).json({
    success:true,
    data: medicine
  })
  
})


exports.createMedicine = asyncHandler(async(req,res,next) => {
  const medicine = await Medicine.create(req.body);

  console.log("fvsdf",medicine)
  res.status(201).json({
    success: true,
    data: medicine
  })
 
}
)


exports.updateMedicine = asyncHandler(async(req,res,next) => {

  const medicine = await Medicine.update(req.body, {
    where: {id: req.params.id},
    returning:true
  })
  const newMedicine = await Medicine.findByPk(req.params.id);

  if(!newMedicine){
    return next(new ErrorResponse(`Medicine with ${req.params.id} not found`,404))
  }
  res.status(200).json({
    success:true,
    data: newMedicine
  })
  
})



exports.deleteMedicine = asyncHandler(async(req,res,next) => {

  const medicine = await Medicine.findByPk(req.params.id);
  if(!medicine) {
    return next(new ErrorResponse(`Medicine with ${req.params.id} not found`,404))
  }
  await medicine.destroy();
  res.status(200).json({
    success: true,
    data: {}
  })

})