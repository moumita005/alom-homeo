const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utlis/errorResponse');
const User = require( '../models/User' );


// @desc        Register User
// @route       POST /api/auth/register
// @acess       Public
exports.register =  asyncHandler(async(req,res,next) =>{
  const { name, email, password, role } = req.body;

  //create user
  const user = await User.create({ name, email, password, role });
  
  
  sendTokenResponse(user, 200, res)

})



// @desc        Login User
// @route       POST /api/auth/login
// @acess       Public
exports.login =  asyncHandler(async(req,res,next) =>{
  const {  email, password } = req.body;
  //validate email and password
  if(!email || !password){
    return next(new ErrorResponse('please provide an email and password',400));
  }


  //check for user
  const user = await User.findOne({ where: {email}, attributes: {include: 'password'} });


  if(!user){
    return next(new ErrorResponse(`Invalid credentials`, 401))
  }

  //check if passwor dmatches
  const isMatch = await user.matchPassword(password);

  if(!isMatch){
    return next(new ErrorResponse(`Invalid credentials`, 401))
  }
  
  
  // //create token
  // const token = user.getSignedJwtToken();
  // res.status(200).json({
  //   success: true,
  //   token:token
  // })

  sendTokenResponse(user, 200, res)

});




// Get token from model, create cookie and send response
const sendTokenResponse = (user, statusCode, res) => {
  console.log("fdsf")
  // Create token
  const token = user.getSignedJwtToken();
  const options = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000
    ),
    httpOnly: true
  };

  if (process.env.NODE_ENV === 'production') {
    options.secure = true;
  }

  res
    .status(statusCode)
    .cookie('token', token, options)
    .json({
      success: true,
      token
    });
};



// @desc        Get current logged in  User
// @route       GET /api/auth/me
// @acess       Private
exports.getMe = asyncHandler(async(req,res,next) => {
  console.log("dfvgdf",req.user)
  const user = await User.findByPk(req.user.id);

  res.status(200).json({
    success: true,
    data: user
  })
})


// @desc      Log user out / clear cookie
// @route     GET /api/v1/auth/logout
// @access    Private
exports.logout = asyncHandler(async (req, res, next) => {
  res.cookie('token', 'none', {
    expires: new Date(Date.now() + 10 * 1000),
    httpOnly: true
  });

  res.status(200).json({
    success: true,
    data: {}
  });
});