const asyncHandler = require('../middleware/async');
const { Patient, Medicine, Prescription } = require('../models');
const ErrorResponse = require('../utlis/errorResponse');

//@desc            get all the patients
//@route           GET /api/patients
//@access          Public
exports.getPrescriptions = asyncHandler(async(req,res,next) => {
  
  const prescriptions = await Prescription.findAll({
    include: [Patient, Medicine]
  });
  console.log(prescriptions)
  res.status(200).json({
    success:true,
    message:`Show all prescriptions ${prescriptions.length}`,
    data : prescriptions
  })
})


//@desc            get single the patients
//@route           GET /api/patients/:id
//@access          Public

exports.getPrescription = asyncHandler(async(req,res,next) => {
  const prescription = await Prescription.findByPk(req.params.id, {include: [Patient, Medicine]});

  if(!prescription){
    return next(new ErrorResponse(`Prescription with ${req.params.id} not found`, 404))
  }

  res.status(200).json({
    success: true,
    message:`Show single prescription ${req.params.id}`,
    data: prescription
  })
})

//@desc            create new patients
//@route           POST /api/patients
//@access          Private
exports.createPrescription = asyncHandler(async( req,res,next ) => {
 
  const prescription = await Prescription.create(req.body);

  const {medicines=[]} = req.body;
  const newdata = medicines.map(m=>({...m, prescription_id: prescription.id}));
  
  await Medicine.bulkCreate(newdata);
  console.log("create medicine",newdata);

  res.status(201).json({
    success: true,
    message:`prescription created `,
    data: prescription , newdata
  })
  
})

//@desc            update patients
//@route           PUT /api/patients/:id
//@access          Private
exports.updatePrescription = asyncHandler(async(req,res,next) => {

  // return console.log(req.body); // abar den to

  const prescription = await Prescription.update(req.body, {
      where: {id: req.params.id},
      returning: true,
    },
  )

  const {medicines=[]} = req.body;

    medicines.map(async m => {
      await Medicine.update(m, {
        where: {id: m.id},
        returning: true,
      }); 
    })

    console.log("after updating",medicines)
    

  const newPrescription = await Prescription.findByPk(req.params.id);
  if(!newPrescription){
    return  next(new ErrorResponse(`Prescription with ${req.params.id} id not found`, 404))
  }
  
  res.status(200).json({
    success: true,
    message:`prescription updated with id ${req.params.id}`,
    data: newPrescription,medicines
  })
  
})


//@desc            delete patients
//@route           DELETE /api/patients/:id
//@access          Private
exports.deletePrescription = asyncHandler(async(req,res,next) => {

    const prescription = await Prescription.findByPk(req.params.id);
    if(!prescription){
      return next(new ErrorResponse(`prescription with ${req.params.id} not found`,404))
    }
    await prescription.destroy(req.params.id);
    res.status(200).json({
      success: true,
      message:`prescription deleted with id ${req.params.id}`,
      data: {}
    })
  
})


