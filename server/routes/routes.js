const patients = require('./patients');
const prescriptions  = require('./prescriptions')
const medicines = require('./medicines')
const uploads = require('./uploads')
const auth = require('./auth')


module.exports = app => {
  app.use('/api/patients', patients);
  app.use('/api/prescriptions', prescriptions);
  app.use('/api/medicines', medicines);
  app.use('/api/uploads', uploads);
  app.use('/api/auth', auth)
}