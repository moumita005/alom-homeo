const express = require('express');
const {
  getPatients,
  getPatient, 
  createPatient,
  updatePatient,
  deletePatient
} = require('../controller/patients')
const router = express.Router();
const {protect,authorize} = require('../middleware/auth')


router
  .route('/')
  .get(getPatients)
  .post(protect , authorize('Admin', 'Operator'), createPatient );


router
  .route('/:id')
  .get(getPatient)
  .put(protect , authorize('Admin', 'Operator'), updatePatient)
  .delete(protect , authorize('Admin', 'Operator'), deletePatient)





module.exports = router