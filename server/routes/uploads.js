const express = require('express');
const router = express.Router();
const asyncHandler = require('../middleware/async')
const upload = require('../middleware/upload');


const uploads = upload.fields([
  {name: 'image', maxCount: 3 }
]);



const getUploadedFiles = asyncHandler(async(req,res,next)=>{

  res.status(201).send({
    success: true,
    message: `Uploaded file list`,
    data: {}
  });
});


const uploadSingle = asyncHandler(async(req,res,next)=>{
  const data = req.files;
  console.log("erfv",data);

  const newData = Object.entries(data).map((item) => {
    console.log("item",item);
    const [key,value] = item;
    console.log("fvsdfv",value);
    const  firstValue  = value;
    return firstValue 
  })
  console.log(newData);

  res.status(201).send({
    success: true,
    message: `File uploaded`,
    data: newData
  })
})



router.route('/').get(getUploadedFiles);
router.route('/single').post(uploads , uploadSingle);


module.exports = router;