const express = require('express');
const { 
  getMedicines, 
  createMedicine, 
  getMedicine, 
  updateMedicine, 
  deleteMedicine 
} = require('../controller/medicines');
const router = express.Router();
const {protect} = require('../middleware/auth')


router
  .route('/').get(getMedicines).post(protect , createMedicine)

router
  .route('/:id').get(getMedicine).put(protect , updateMedicine).delete(protect , deleteMedicine);


module.exports = router