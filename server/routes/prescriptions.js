const express = require('express');
const { 
  getPrescriptions, 
  createPrescription, 
  getPrescription, 
  updatePrescription, 
  deletePrescription 
} = require('../controller/prescriptions');
const router = express.Router();
const {protect,authorize} = require('../middleware/auth')


router
  .route('/')
  .get(getPrescriptions)
  .post(protect , authorize('Admin', 'Operator', 'User'), createPrescription);


router
  .route('/:id')
  .get(getPrescription)
  .put(protect , authorize('Admin', 'Operator', 'User'),updatePrescription)
  .delete(protect , authorize('Admin', 'Operator', 'User'),deletePrescription)


module.exports = router