const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs');
const {Sequelize, sequelize} = require('../config/db');


const User = sequelize.define('user', {
  id: { 
    type: Sequelize.INTEGER, 
    primaryKey: true, 
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull : { msg: 'Name can not be null' },
      notEmpty: { msg: 'Name can not be null' }
    }
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Email can not be null" }, 
      isEmail: { msg: "Not a valid email" }
    },
    unique: {
      args:true,
      msg: 'Email address already in use!'
    },
  },

  password: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: { msg: "Password can not be null" }, 
      notEmpty: { msg: "Password can not be empty" },
      len: {
        min: 6,
        msg: "At least 6 character long"
      }
    }
  },

  phone: {
    type: Sequelize.STRING,
    // allowNull: false,
    // validate: {
    //   notNull: { msg: "Phone can not be null" }, 
    //   notEmpty: { msg: "Phone can not be empty" },
    //   is: {
    //     args: /^(?:\+88|01)?(?:\d{11}|\d{13})$/i,
    //     msg: "Not a vaild phone number"
    //   }
    // }
  },

  role : {
    type: Sequelize.ENUM,
    values: ['Admin', 'Operator','Pharmacist', 'User'], // Suprer admin can only be set manually
    defaultValue: 'User'
  },
  designation: Sequelize.STRING,
  // status: {
  //   type: Sequelize.ENUM,
  //   values: ['Pending', 'Active', 'Inactive'],
  //   defaultValue: 'Active'
  // },
  reset_password_token: Sequelize.STRING,
  reset_password_expire: Sequelize.DATE
},
{
  defaultScope: {
    attributes: {
      exclude: ['password']
    }
  }
}
);


//encrypt password using bcrypt
User.beforeSave(async (user) => {
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);
});



// Sign jwt and return
User.prototype.getSignedJwtToken = function(){
  return jwt.sign({
    id:this.id,
    email: this.email,
    role: this.role
  }, process.env.JWT_SECRET,{
    expiresIn: process.env.JWT_EXPIRE
  })
}


//Match user entered password to hashed password entered in database
User.prototype.matchPassword =async function(enteredPassword){
  return await bcrypt.compare(enteredPassword, this.password)
}


//


module.exports = User;