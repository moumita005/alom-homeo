const {Sequelize, sequelize} = require('../config/db');

const Patient = sequelize.define('patient', {
  id: { 
    type: Sequelize.INTEGER, 
    primaryKey: true, 
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull:false,
    validate: {
      notNull: { msg: "Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },
  father_name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notNull: "Father name can not be null",
      notEmpty: "Father name can not be null"
    }
  },
  health:Sequelize.STRING,
  skin_color: Sequelize.STRING,
  profession: Sequelize.STRING,
  age: Sequelize.STRING,
  weight:Sequelize.STRING,
  gender:Sequelize.STRING,
  address:Sequelize.STRING,
  page_no:Sequelize.STRING,
  reg_no: Sequelize.STRING,
  case_no: Sequelize.STRING,
  sibling: Sequelize.STRING,
  children:Sequelize.STRING,
  is_father_alive:Sequelize.STRING,
  is_mother_alive:Sequelize.STRING,
  is_partner_alive:Sequelize.STRING,
  allergy:Sequelize.STRING,
  scare: Sequelize.STRING,
  cancer: Sequelize.STRING,
  blood_pressure: Sequelize.STRING,
  sleep:Sequelize.STRING,
  mental_illness:Sequelize.STRING,
  family:Sequelize.STRING,
  present:Sequelize.STRING,
  self: Sequelize.STRING,
  description: Sequelize.STRING,
  date: Sequelize.STRING,
  
})


module.exports = Patient;