const {Sequelize, sequelize} = require('../config/db');

const Prescription = sequelize.define('prescription', {
  id: { 
    type: Sequelize.INTEGER, 
    primaryKey: true, 
    autoIncrement: true
  },
  patient_id: Sequelize.INTEGER,
  date: Sequelize.STRING,
  less: Sequelize.STRING,
  status:{
    type: Sequelize.STRING,
    defaultValue: 'Pending'
  },
})


module.exports = Prescription;