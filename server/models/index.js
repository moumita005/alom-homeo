const Prescription = require('./Prescription');
const Patient = require('./Patient');
const Medicine = require('./Medicine');



Patient.hasMany(Prescription);
Prescription.belongsTo(Patient);

Prescription.hasMany(Medicine);
Medicine.belongsTo(Prescription)



module.exports = {
  Prescription,
  Patient,
  Medicine
}