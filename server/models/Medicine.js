const {Sequelize, sequelize} = require('../config/db');

const Medicine = sequelize.define('medicine', {
  id: { 
    type: Sequelize.INTEGER, 
    primaryKey: true, 
    autoIncrement: true
  },
  medicine_name: {
    type: Sequelize.STRING,
    allowNull:false,
    validate: {
      notNull: { msg: "Medicine Name can not be null" }, 
      notEmpty: { msg: "Name can not be empty" },
    }
  },
  medicine_type: {
    type: Sequelize.STRING,
    
  },
  day:Sequelize.STRING,
  instruction: Sequelize.STRING,
  quantity: Sequelize.STRING,
  unit_price: Sequelize.STRING,
  prescription_id: Sequelize.INTEGER
  
})


module.exports = Medicine;