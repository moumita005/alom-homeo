const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const cookieParser = require('cookie-parser')
const errorHandler = require('./middleware/error')


//load env vars 
dotenv.config({path: './config/config.env'});

//connect to database
const {connectSQL} = require('./config/db')


const app = express();

//Body parser
app.use(express.json())

// cookie parser
app.use(cookieParser())

//Dev logging middleware
if(process.env.NODE_ENV==='development'){
  app.use(morgan('dev'))
}

if (process.env.NODE_ENV !== 'test') {
  connectSQL()
}

//route files
const mountRoute = require('./routes/routes')



//Mount routers
mountRoute(app);


app.use(errorHandler)


const PORT = process.env.PORT || 3900;


const server = app.listen(PORT, console.log(`server running in ${process.env.NODE_ENV} mode on  port ${PORT}`.yellow.bold));

//Handle unhandled rejections
process.on('unhandledRejection', (err,promise)=>{
  console.log(`Error : ${err.message}`.red);
  //close server process
  server.close(()=> process.exit(1))
})