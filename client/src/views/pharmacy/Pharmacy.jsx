import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import { Modal,Tag } from 'antd'
import { deletePrescription, getPrescription, getPrescriptions,savePrescription } from '../../services/prescription';
import icon from '../../img/icon.png'
import moment from 'moment'



const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };
  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}
export default class Pharmacy extends Component {
  state={
    data: {},
    prescriptions: [
      // {id:1, name: 'mou' , status: 'pending', medicine_name: 'napa', day: 7},
      // {id:2, name: 'moin' , status: 'pending',medicine_name: 'napa', day: 7},
      // {id:3, name: 'pantho' ,status: 'pending' ,medicine_name: 'napa', day: 7},
      // {id:4, name: 'himel' ,status: 'pending' ,medicine_name: 'napa', day: 7},
      // {id:5, name: 'parvez' ,status: 'pending',medicine_name: 'napa', day: 7 },
      // {id:6, name: 'supto' , status: 'pending', medicine_name: 'napa', day: 7},
      // {id:7, name: 'ismity' ,status: 'pending' , medicine_name: 'napa', day: 7},
      // {id:8, name: 'faria' ,status: 'pending', medicine_name: 'napa', day: 7 },
      // {id:9, name: 'rafi' , status: 'pending', medicine_name: 'napa', day: 7 },
      // {id:10, name: 'shakal' ,status: 'pending', medicine_name: 'napa', day: 7 },
  
  ],
    rowsPerPage:8,
    search:'',
    page:0,
    dispenceShow: false,
    
  }
  
  async componentDidMount(){
    const {data:{data:prescriptions}} = await getPrescriptions();
    // console.log(prescriptions);
    this.setState({ prescriptions })
  }

  handleChange = (e) => {
    this.setState({search: e.target.value})
  }

  handleDataChange = (e) => {
    const data = {...this.state.data};
    data[e.target.name] = e.target.value;
    this.setState({data})
  }

  handleArrayChange= ({target}, arrName, index) => {
    const data = {...this.state.data}
    const arr = data[arrName]
    arr[index][target.name] = target.value
    data[arrName]=arr
    this.setState({data})
  }


  handleDeleteShow = (data) =>{
    this.setState({data, deleteShow: true})
  }

  handleDelete = async(e) => {
    e.preventDefault();
    const {data} = this.state;
    console.log(data.id)
    await deletePrescription(data.id);
    const {data:{data:prescriptions}} = await getPrescriptions();
    console.log(prescriptions);
    this.setState({prescriptions,deleteShow:false})
  }


  handleChangePage =(e, newPage) => {
    // console.log("before",newPage)
    this.setState({page:newPage})
    // console.log("after",newPage)
  }
  
  handleChangeRowsPerPage = (e) => {
  
    this.setState({rowsPerPage:e.target.value,page:0})
    
  }

  handleViewShow = (data) => {
    this.setState({data, viewShow: true})
  }
 
  handleView = async () => {
     const {data} = this.state
     console.log(data);
     this.setState({viewShow:false})
    //  this.setState({data: payment.data.data, viewShow:false})
     
    //  console.log(payment.data.data)
  }

  handleDispenceShow = (data) => {
    this.setState({data, dispenceShow: true})
  }
 
  handleDispence = async (e) => {
    
     const {data} = this.state
     let totalAmount
     console.log(data);
     
     data.status = 'Dispensed'
     await savePrescription(data)
     const {data:{data:prescription}} = await getPrescription(data.id)
     console.log(prescription)
     window.location = "/#/prescriptions"
     this.setState({dispenceShow:false,data:prescription})
    //  this.setState({data: payment.data.data, viewShow:false})
     
    //  console.log(payment.data.data)
  }
  
  handleClose=() =>{
    this.setState({data:{}, dispenceShow:false, viewShow:false, deleteShow:false })
  }

  render() {
    let subTotal = 0 
    let totalPrice = 0
    const {prescriptions,page,rowsPerPage,deleteShow, dispenceShow,data,search,less,viewShow} = this.state
    return (
      <div>
        <div className="">
          {/* <div className="card-header">
            <span className="na-text" style={{fontSize:'30px'}}></span>
            <Link to= '/pharmacies'><button className="btn btn-sm btn-success float-right"><span className="font-weight-bold">+ Add pharmacy</span></button></Link>
          </div> */}
          <div className="">
            <div className="row">
              <div className="col-md-10"></div>
              <div className="col-md-2">
                <input 
                  name="search"
                  value={this.state.search}
                  type="text" 
                  className="form-control my-4 " 
                  placeholder="Search...." 
                  onChange={this.handleChange}
                />

              </div>

            </div>
            <TableContainer  component={Paper}>
              <Table  aria-label="simple table" size="small" >
                <TableHead  style={{background: '#4288bf '}} >
                  <TableRow>
                    <TableCell className="font-weight-bold text-white" align="left" >Date</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Patient Id</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Patient Name</TableCell>
                    <TableCell className="font-weight-bold text-white" align="center">Status</TableCell>
                    {/* <TableCell className="font-weight-bold text-white" align="left" style={{border: '1px solid black'}}>Phone no</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" style={{border: '1px solid black'}}>Guardian Phone no</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" style={{border: '1px solid black'}}>Educational Qualification</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" style={{border: '1px solid black'}}>Local Guardian Phone</TableCell> */}
                    <TableCell className="font-weight-bold text-white" align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody >
                  {(rowsPerPage > 0
                      ? prescriptions.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      : prescriptions
                    ).filter(val=>{
                      if(search == '') {
                        return val
                      }else if( val.name.toLowerCase().includes(search.toLowerCase())  ){
                        return val
                      }
                    }).map((prescription,index) => (
                    <TableRow key={index} className="tr">
                      <TableCell align="left">{prescription.date && moment(prescription.date).format("DD/MM/YYYY")} </TableCell>
                      <TableCell align="left">{prescription.patient && prescription.patient.id} </TableCell>
                      <TableCell align="left">{prescription.patient && prescription.patient.name} </TableCell>
                      <TableCell align="center">{prescription.status === 'Pending'? (
                        <Tag color="gold" style={{fontSize: '12px', fontWeight: '900'}}>{prescription.status}</Tag>
                        ) : (
                        <Tag color="geekblue" style={{fontSize: '12px', fontWeight: '900'}}>{prescription.status}</Tag>
                        ) }
                      </TableCell>
                      {/* <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.email}</TableCell>
                      <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.phone_no}</TableCell>
                      <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.guardian_phone_no}</TableCell>
                      <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.educational_qualification}</TableCell>
                      <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.local_guardian_phone_no}</TableCell> */}
                      <TableCell align="center"  >
                        <i 
                          className="fa fa-eye mr-3 text-primary icon" 
                          onClick = {()=> {
                            return(this. handleViewShow(prescription))}}
                         
                        ></i>
                        {/* <Link to={`/student/${student._id}`}>
                          <i 
                            className="fa fa-pencil-square-o text-success mr-3"
                          ></i>
                        </Link> */}
                        {
                          prescription.status === 'Pending' ? (
                            <button
                              onClick = {()=> {
                                return(this. handleDispenceShow(prescription))}}
                              className="btn btn-sm  mr-3 icon"
                              title="dispense"
                            >
                              <img src={icon} style={{width:'18px', height: '18px'}} />
                            </button>
                           ) : (
                            <button
                              onClick = {()=> {
                                return(this. handleDispenceShow(prescription))}}
                              className="btn btn-sm  mr-3 icon"
                              title="dispense"
                              disabled
                            >
                              <img src={icon} style={{width:'18px', height: '18px'}} />
                            </button>
                           )
                        }
                        
                        <i 
                          className="fa fa-trash-o clickable text-danger icon" 
                          onClick={() => this.handleDeleteShow(prescription)} 
                          title="delete"
                        ></i>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[ 10, 25, { label: 'All', value: -1 }]}
                      colSpan={3}
                      count={prescriptions.length}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      SelectProps={{
                        inputProps: { 'aria-label': 'rows per page' },
                        native: true,
                      }}
                      onChangePage={this.handleChangePage}
                      onChangeRowsPerPage={this.handleChangeRowsPerPage}
                      ActionsComponent={TablePaginationActions}
                    />
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>
            {/* <Modal
              visible={dispenceShow}
              onOk= {this.handleDispence}
              onCancel={this.handleClose}
              width={900}
            >
              <div>
                
                <h5 className="mt-3 text-center lead"></h5>
                <div className="row">
                  <div className="col-md-6">
                    <p>Patient Name: {data.patient && data.patient.name}</p>
                  </div>
                  <div className="col-md-6"></div>
                </div>
                <table  className="mt-2 table table-striped table-bordered table-responsive-sm w-100">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Status</th>
                      
                    </tr>
                </thead>
                <tbody>
                  {data.medicines && data.medicines.map((med,index)=>{
                    return(
                      <tr key={index}>
                        <td>{med.medicine_name}</td>
                        <td>{med.status}</td>
                      </tr> 

                    )
                  })}
                       
                  </tbody>
                </table>
                
              </div>
            </Modal>  */}
            <Modal
              visible={deleteShow}
              onOk = {this.handleDelete}
              onCancel={this.handleClose}
              okText = "Delete"
              okType = 'danger'
            >
              <p>Are you sure you want to <span className="text-danger font-bold">delete</span> this item?</p>
              <div className="text-right">
                {/* <button className="btn btn-sm btn-light mx-3" onClick={this.handleClose}>Cancel</button>
                <button className="btn btn-sm btn-danger" onClick={this.handleDelete}>Delete</button> */}

              </div>
            </Modal>
            <Modal
              visible={dispenceShow}
              onOk= {this.handleDispence}
              onCancel={this.handleClose}
              okText = "Submit"
              width={1200}
              // okType = 'danger'
            >
             
              <div>
                
                <h5 className="mt-3 text-center lead"></h5>
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      {/* <div className="card-header" style={{background: '#4288bf '}}>
                        <div className="h5"><span style={{borderBottom: '1px solid white' , color: 'white'}}>Patient Info</span></div>
                      </div> */}
                      <div  style={{background: '#4288bf '}}>
                        <p style={{fontSize: '16px'}} className="pt-3 pl-3 text-white"><span className="font-weight-bold"> Name: </span>{data.patient && data.patient.name}, 
                        <span className="font-weight-bold pl-3"> Age: </span>{data.patient && data.patient.age},
                        <span className="font-weight-bold pl-3"> Weight: </span>{data.patient && data.patient.weight},
                        <span className="font-weight-bold pl-3"> Gender: </span>{data.patient && data.patient.gender},
                        <span className="font-weight-bold pl-3"> Case No: </span>{data.patient && data.patient.case_no},
                        <span className="font-weight-bold pl-3"> Registration No: </span>{data.patient && data.patient.reg_no}</p>
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
                <div className="h5 mt-3 text-center"><span style={{borderBottom: '1px solid black'}}>Medicine</span></div>
                <table  className="mt-2 table  table-hover table-sm table-responsive-sm w-100">
                  <thead  className="" style={{background: '#D9EDF7 ', color: '#3C4767'}}>
                    <tr>
                      <th>Medicine Name</th>
                      <th>Quantity</th>
                      <th>Unit Price</th>
                      <th>Total Price</th>

                      {/* <th> Type</th>
                      <th> Days</th>
                      <th> Instructions</th> */}
                      
                    </tr>
                </thead>
                <tbody>
                  {data.medicines && data.medicines.map((med,index)=>{
                    subTotal += (med.quantity || '')*(med.unit_price || '');
                    
                    return(
                      <tr key={index}>
                        <td>{med.medicine_name}</td>
                        <td>
                          <input
                            name="quantity"
                            value={med.quantity || ''}
                            onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                            className="form-control form-control-sm" 
                            required
                          />
                        </td>
                        <td>
                          <input
                            name="unit_price"
                            value={med.unit_price || ''}
                            onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                            className="form-control form-control-sm" 
                            required
                          />
                        </td>
                        <td>{((med.quantity || '')*(med.unit_price || '')).toFixed(2)}</td>
                        {/* <td>{med.instruction}</td> */}
                      </tr> 

                    )
                  })}
                       
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colSpan="2"></td>
                      <td className="text-right">Sub Total</td>
                      <td> {subTotal.toFixed(2)} </td>
                    </tr>
                  
                    <tr>
                      <td colSpan="2"></td>
                      <td className="text-right">less</td>
                      <td> 
                        <input 
                          name="less" 
                          value={data.less || ''} 
                          // onChange={({target}) => this.setState({less: target.value})} 
                          onChange={this.handleDataChange} 
                        />
                      </td>
                    </tr>
                    <tr>
                      <td colSpan="2"></td>
                      <td className="text-right">Total Price</td>
                      <td> 
                        {(( subTotal || '')-( data.less || '')).toFixed(2) || ''}
                      </td>
                    </tr>
                  </tfoot>
                </table>
                
              </div>
            </Modal> 
            <Modal
              visible={viewShow}
              onOk= {this.handleView}
              onCancel={this.handleClose}
              width={1600}
            >
              <div>
                
                <h5 className="mt-3 text-center lead"></h5>
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      {/* <div className="card-header" style={{background: '#4288bf '}}>
                        <div className="h5"><span style={{borderBottom: '1px solid white' , color: 'white'}}>Patient Info</span></div>
                      </div> */}
                      <div  style={{background: '#4288bf '}}>
                        <p style={{fontSize: '16px'}} className="pt-3 pl-3 text-white"><span className="font-weight-bold"> Name: </span>{data.patient && data.patient.name}, 
                        <span className="font-weight-bold pl-3"> Age: </span>{data.patient && data.patient.age},
                        <span className="font-weight-bold pl-3"> Weight: </span>{data.patient && data.patient.weight},
                        <span className="font-weight-bold pl-3"> Gender: </span>{data.patient && data.patient.gender},
                        <span className="font-weight-bold pl-3"> Case No: </span>{data.patient && data.patient.case_no},
                        <span className="font-weight-bold pl-3"> Registration No: </span>{data.patient && data.patient.reg_no}</p>
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
                <div className="h5 mt-3 text-center"><span style={{borderBottom: '1px solid black'}}>Medicine</span></div>
                <table  className="mt-2 table  table-hover table-sm table-responsive-sm w-100">
                  <thead  className="" style={{background: '#D9EDF7 ', color: '#3C4767'}}>
                    <tr>
                      <th>Medicine Name</th>
                      <th>Quantity</th>
                      <th>Unit Price</th>
                      <th>Total Price</th>

                      {/* <th> Type</th>
                      <th> Days</th>
                      <th> Instructions</th> */}
                      
                    </tr>
                </thead>
                <tbody>
                  {data.medicines && data.medicines.map((med,index)=>{
                    
                    return(
                      <tr key={index}>
                        <td>{med.medicine_name}</td>
                        <td>{med.quantity || ''}</td>
                        <td>{med.unit_price || ''}</td>
                        <td>{((med.quantity || '')*(med.unit_price || '')).toFixed(2)}</td>
                        {/* <td>{med.instruction}</td> */}
                      </tr> 

                    )
                  })}
                       
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colSpan="2"></td>
                      <td className="text-right">Sub Total</td>
                      <td> {subTotal.toFixed(2)} </td>
                    </tr>
                  
                    <tr>
                      <td colSpan="2"></td>
                      <td className="text-right">less</td>
                      {console.log("sdrfvsrf",data.less)}
                      <td> {data.less || null}  </td>
                    </tr>
                    <tr>
                      <td colSpan="2"></td>
                      <td className="text-right">Total Price</td>
                      <td> 
                        {(( subTotal || '')-( data.less || '')).toFixed(2) || ''}
                      </td>
                    </tr>
                  </tfoot>
                </table>
                {/* <div className="text-right">
                  {data.medicines && data.medicines.map((medicine,index)=>{
                    console.log("erfewr",medicine.quantity, medicine.unit_price)
                    medicine.quantity && medicine.unit_price?(<button className="btn" onClick={this.handleDispence}>Delete</button>):(<button className="btn btn-sm btn-danger" onClick={this.handleDispence} disabled>Delete</button>)
                  })}

                </div> */}
              </div>
            </Modal>
          </div>
        </div>
      </div>
    )
  }
}
