// import React, { Component } from 'react';
// import { Form, Input, Button, Radio ,Upload, DatePicker  } from 'antd';
// // import { UploadOutlined } from '@ant-design/icons';
// // import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// // import ckeditor, { CKEditor } from '@ckeditor/ckeditor5-react'
// import { Link } from 'react-router-dom';
// import { getPatient, savePatient } from '../../services/patient';

// const { TextArea } = Input;

// const layout = {
//   labelCol: {
//     span: 6,
//   },
//   wrapperCol: {
//     span: 13,
//     offset: 1,
//   },
// };
// const tailLayout = {
//   wrapperCol: {
//     offset: 7,
//     span: 16,
//   },
// };
// export default class AddNewPatientOld extends Component {
  
//   state={
//     data:{},
//     errors:{},
//     description:'',
    
//   }
  
//   async componentDidMount(){
//     const patient_id = this.props.match.params.id

//     if(patient_id === 'new') return
//     const {data:{data}} = await getPatient(patient_id);
//     console.log(data)
//     this.setState({data})

//   }

//   handleChange = ({target}) => {
//     const data = {...this.state.data}
//     const errors = {...this.state.errors}
//     data[target.name] = target.value;
//     delete errors[target.name];
//     this.setState({data,errors})
//   }

//   handleCkeditorState = (e,editor) => {
    
//     const data= editor.getData();
//     console.log(data)
//     this.setState({ description:data })
//   }

//   handleSubmit = async () => {
//     const {data,errors} = this.state;
//     try{
//       const patient = await savePatient(data)
//       console.log(patient)
//       this.setState({data:{}})
//     }catch(err){
//       if(err.response && err.response.status === 400){
//         this.setState({errors: err.response.data.errors});
//       }
//       else{
//         console.error(err.response.data.error)
//       }
//     }
   
//   }
//   render() {
//     const {data} = this.state
//     return (
//       <div className="container-fluid">
//         <div className="card add_new">
//           <div className="card-header ">
//             <Link to={`/patients`}><button className="btn " style={{background: '#4288bf', color:'white'}}><i className="fas fa-list pr-2"></i>Patient List</button></Link>
//           </div>
//           <div className="card-body">
//             <div className="row">
//               <div className="col-md-12">

//                 <Form
//                   {...layout}
//                   name="basic"
//                   onFinish = {this.handleSubmit}
//                 >
//                   {/* <Form.Item
//                     label="Id"
//                     name="id"
//                     rules={[
//                       {
//                         required: true,
//                         message: 'Please input your username!',
//                       },
//                     ]}
//                   >
//                     <Input 
//                       name="id"
//                       value={data.id || ''}
//                       onChange={this.handleChange} 
//                     />
//                   </Form.Item> */}

//                   <Form.Item
//                     label="Patient Name"
//                     name="name"
//                     onChange={this.handleChange} 
//                     rules={[
//                       {
//                         required: true,
//                         message: 'Please input your name!',
//                       },
//                     ]}
//                   >
//                     <Input
//                       name="name"
//                       onChange={this.handleChange} 
//                     />
//                   </Form.Item>

//                   <Form.Item
//                     label="Father Name"
//                     name="father_name"
//                     value={data.father_name || ''}
                    
//                   >
//                     <Input 
//                       name="father_name"
//                       value={data.father_name || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>
//                   <Form.Item
//                     label="Health"
//                     name="health"
//                     value={data.health || ''}
//                   >
//                     <Input 
//                       name="health"
//                       value={data.health || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>
//                   <Form.Item
//                     label="Color of Skin"
//                     name="skin_color"
//                     value={data.skin_color || ''}
//                   >
//                     <Input 
//                       name="skin_color"
//                       value={data.skin_color || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>

//                   <Form.Item
//                     label="Profession"
//                     name="profession"
                    
//                   >
//                     <Input 
//                       name="profession"
//                       value={data.profession || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>

//                   <Form.Item
//                     label="Age"
//                     name="age"
//                     rules={[
//                       {
//                         required: true,
//                         message: 'Please input your age!',
//                       },
//                     ]}
//                   >
//                     <Input 
//                       name="age"
//                       value={data.age || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>
                  
//                   <Form.Item
//                     label="Weight"
//                     name="weight"
//                     rules={[
//                       {
//                         required: true,
//                         message: 'Please input your weight!',
//                       },
//                     ]}
//                   >
//                     <Input 
//                       name="weight"
//                       value={data.weight || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>

//                   <Form.Item
//                     label="Gender"
//                     name="gender"
//                     rules={[
//                       {
//                         required: true,
//                         message: 'Please input your gender!',
//                       },
//                     ]}
//                   >
//                     <Radio.Group 
//                       name="gender"
//                       value={data.gender || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="male">Male</Radio>
//                       <Radio value="female">Female</Radio>
//                     </Radio.Group>
//                   </Form.Item>
//                   <Form.Item
//                     label="Address"
//                     name="address"
//                     rules={[
//                       {
//                         required: true,
//                         message: 'Please input your address!',
//                       },
//                     ]}
//                   >
//                     <Input 
//                       name="address"
//                       value={data.address || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>
              
//                   <Form.Item
//                     label="Page no"
//                     name="page_no"
                    
//                   >
//                     <Input 
//                       name="page_no"
//                       value={data.page_no || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>
//                   <Form.Item
//                     label="Register no"
//                     name="reg_no"
                    
//                   >
//                     <Input 
//                       name="reg_no"
//                       value={data.reg_no || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>

//                   <Form.Item
//                     label="Case no"
//                     name="case_no"
                    
//                   >
//                     <Input 
//                       name="case_no"
//                       value={data.case_no || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>

//                   <Form.Item
//                     label="Date"
//                     name="date"
                    
//                   >
//                     {/* <input 
//                       name="date"
//                       value={data.date || ''}
//                       onChange={this.handleChange} 
//                       className="form-control" 
//                       type="date" 
//                     /> */}
//                     <DatePicker 
//                       style={{width: "100%"}}
//                     />
//                   </Form.Item>

//                   <Form.Item
//                     label="How many siblings do you have?"
//                     name="sibling"
                    
//                   >
//                     <Input 
//                       name="sibling"
//                       value={data.sibling || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>
//                   <Form.Item
//                     label="How many children do you have?"
//                     name="children"
                    
//                   >
//                     <Input 
//                       name="children"
//                       value={data.children || ''}
//                       onChange={this.handleChange}  
//                     />
//                   </Form.Item>
//                   <Form.Item
//                     label="Is your Fahter alive?"
//                     name="is_father_alive"
//                   >
//                     <Radio.Group
//                       name="is_father_alive"
//                       value={data.is_father_alive || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="yes">Yes</Radio>
//                       <Radio value="no">No</Radio>
//                     </Radio.Group>
//                   </Form.Item>

//                   <Form.Item
//                     label="Is your Mother alive?"
//                     name="is_mother_alive"
                    
//                   >
//                     <Radio.Group
//                       name="is_mother_alive"
//                       value={data.is_mother_alive || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="yes">Yes</Radio>
//                       <Radio value="no">No</Radio>
//                     </Radio.Group>
//                   </Form.Item>

//                   <Form.Item
//                     label="Is your Husband/Wife alive?"
//                     name="is_partner_alive"
                    
//                   >
//                     <Radio.Group
//                       name="is_partner_alive"
//                       value={data.is_partner_alive || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="yes">Yes</Radio>
//                       <Radio value="no">No</Radio>
//                     </Radio.Group>
//                   </Form.Item>

//                   <Form.Item
//                     label="Do you have any allergy?"
//                     name="allergy"
                    
//                   >
//                     <Radio.Group
//                       name="allergy"
//                       value={data.allergy || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="yes">Yes</Radio>
//                       <Radio value="no">No</Radio>
//                     </Radio.Group>
//                   </Form.Item>

//                   <Form.Item
//                     label="Do you have blood pressure?"
//                     name="pressure"
                    
//                   >
//                     <Radio.Group
//                       name="pressure"
//                       value={data.pressure || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="yes">Yes</Radio>
//                       <Radio value="no">No</Radio>
//                     </Radio.Group>
//                   </Form.Item>

//                   <Form.Item
//                     label="Do you sleep properly at night?"
//                     name="sleep"
                    
//                   >
//                     <Radio.Group
//                       name="sleep"
//                       value={data.sleep || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="yes">Yes</Radio>
//                       <Radio value="no">No</Radio>
//                     </Radio.Group>
//                   </Form.Item>

//                   <Form.Item
//                     label="Does anybody has mental illness in your family?"
//                     name="mental_illness"
                    
//                   >
//                     <Radio.Group
//                       name="mental_illness"
//                       value={data.mental_illness || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="yes">Yes</Radio>
//                       <Radio value="no">No</Radio>
//                     </Radio.Group>
//                   </Form.Item>

//                   <Form.Item
//                     label="Does anybody has cancer in your family?"
//                     name="cancer"
                    
//                   >
//                     <Radio.Group
//                       name="cancer"
//                       value={data.cancer || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="yes">Yes</Radio>
//                       <Radio value="no">No</Radio>
//                     </Radio.Group>
//                   </Form.Item>

//                   <Form.Item
//                     label="Do you get scared easily?"
//                     name="scare"
                    
//                   >
//                     <Radio.Group
//                       name="scare"
//                       value={data.scare || ''}
//                       onChange={this.handleChange} 
//                     >
//                       <Radio value="yes">Yes</Radio>
//                       <Radio value="no">No</Radio>
//                     </Radio.Group>
//                   </Form.Item>

//                    <Form.Item
//                     label="Family"
//                     name="family"
                    
//                   >
//                     <TextArea 
//                       rows={4} 
//                       name="family"
//                       value={data.family || ''}
//                       onChange={this.handleChange}
//                     />
                    
//                   </Form.Item>

//                   <Form.Item
//                     label="Present"
//                     name="present"
                    
//                   >
//                     <TextArea 
//                       rows={4} 
//                       name="present"
//                       value={data.present || ''}
//                       onChange={this.handleChange}
//                     />
                    
//                   </Form.Item>

//                   <Form.Item
//                     label="Self"
//                     name="self"
                    
//                   >
//                     <TextArea 
//                       rows={4} 
//                       name="self"
//                       value={data.self || ''}
//                       onChange={this.handleChange}
//                     />
                    
//                   </Form.Item>

//                   <Form.Item
//                     label="Description"
//                     name="description"
                    
//                   >
//                     <TextArea 
//                       rows={4}
//                       name="description"
//                       value={data.description || ''}
//                       onChange={this.handleChange}
//                     />
//                    {/* <CKEditor
//                       editor = {ClassicEditor}
//                       onInit = {editor =>{}
//                       }
//                       name="description"
//                       value={this.state.description}
//                       onChange={this.handleCkeditorState}
                      
//                     />  */}
//                   </Form.Item> 

//                   {/* <Form.Item
//                     label="Image"
//                     name="image"
                    
//                   >
//                     <Upload
//                       name="image"
//                     >
//                       <Button><UploadOutlined /> Click to Upload</Button>
//                     </Upload>
//                   </Form.Item> */}
                  
//                   <Form.Item {...tailLayout}>
//                     <Button  
//                       htmlType="submit" 
//                       style={{background: '#4288bf', color:'white'}}>
//                       Submit
//                     </Button>
//                   </Form.Item>
//                 </Form>
//               </div>
//             </div>

//           </div>
//         </div>
//       </div>
//     )
//   }
// }
