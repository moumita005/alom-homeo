import React, { Component } from 'react';
import auth from '../../../services/authService'

export default class Login extends Component {
  state={
    data: {},
    error: ''
  }
  
  componentDidMount() {
    const user = auth.getCurrentUser();
    if(user) this.props.history.push('/');
  } 

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    delete errors[input.name];
    data[input.name] = input.value;
    this.setState({ data, errors, error: null });
  }
  
  handleSubmit = async(e) => {
    const {data,error} = this.state;
    if(error) return;
    console.log(data)
    try{
      await auth.login(data);
      window.location = "/"
    }catch(err){
      if(err.response && (err.response.status === 400 || err.response.status === 401)){
        this.setState({error: err.response.data.error})
      }
    }
  }
  render() {
    const {data, error} = this.state
    return (
      <div>
        <div className="login-wrap mt-5">
          <div className="login-html">
            <input id="tab-1" type="radio" name="tab" className="sign-in" checked /><label for="tab-1" className="tab font-weight-bold">Sign In</label>
            <input id="tab-2" type="radio" name="tab" className="sign-up"/><label for="tab-2" className="tab font-weight-bold" >
              {/* Sign Up */}
              </label>
            <div className="login-form">
              <div className="sign-in-htm">
                <div className="group">
                  <label for="user" className="label" style={{color: 'white'}}>Username</label>
                  <input 
                    name="email"
                    value={data.email || ''}
                    onChange = {this.handleChange}
                    id="user" 
                    type="text" 
                    className="input" 
                  />
                </div>
                <div className="group">
                  <label for="pass" className="label" style={{color: 'white'}}>Password</label>
                  <input 
                    name="password"
                    value={data.password || ''}
                    onChange = {this.handleChange}
                    id="pass" 
                    type="password" 
                    className="input" 
                    data-type="password" 
                  />
                </div>
                {/* <div className="group">
                  <input id="check" type="checkbox" className="check" checked />
                  <label for="check"><span className="icon"></span> Keep me Signed in</label>
                </div> */}
                <div className="group">
                  <input 
                    type="submit" 
                    className="button" 
                    value="Sign In" 
                    onClick={this.handleSubmit}
                  />
                </div>
                {/* <hr /> */}
                {/* <div className="foot-lnk">
                  <a href="#forgot">Forgot Password?</a>
                </div> */}
              </div>
              {/* <div className="sign-up-htm">
                <div className="group">
                  <label for="user" className="label" style={{color: 'white'}}>Username</label>
                  <input 
                    name="name"
                    value={data.name || ''}
                    onChange = {this.handleChange}
                    id="user" 
                    type="text" 
                    className="input" 
                  />
                </div>
                <div className="group">
                  <label for="pass" className="label" style={{color: 'white'}}>Password</label>
                  <input 
                    name="password"
                    value={data.password || ''}
                    onChange={this.handleChange}
                    id="pass" 
                    type="password" 
                    className="input" 
                    data-type="password" 
                  />
                </div>
                <div className="group">
                  <label for="pass" className="label" style={{color: 'white'}}>Phone</label>
                  <input 
                    name="phone"
                    value={data.phone || ''}
                    onChange={this.handleChange}
                    id="pass" 
                    type="password" 
                    className="input" 
                    data-type="password" 
                  />
                </div>
                <div className="group">
                  <label for="pass" className="label" style={{color: 'white'}}>Email Address</label>
                  <input 
                    name="email"
                    value={data.email || ''}
                    onChange={this.handleChange}
                    id="pass" 
                    type="email" 
                    className="input" 
                  />
                </div>
                <div className="group">
                  <input type="submit" className="button" value="Sign Up" />
                </div>
                <div className="hr"></div>
                
              </div> */}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
