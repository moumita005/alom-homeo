import React, {useState,useEffect} from 'react';
import { Link } from 'react-router-dom';
import { Form, Input, Button, Radio ,Upload, DatePicker  } from 'antd';
import { getPatient, savePatient } from '../../services/patient';

const { TextArea } = Input;

const layout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 13,
    offset: 1,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 7,
    span: 16,
  },
};

 const AddNewPatient = (props) => {
  const [form] = Form.useForm();
  const [data, setData] = useState({});
  const [errors, setErrors] = useState({})
  
  useEffect(() => {
    (
      async () => {
        console.log("Effect 1");
        const patient_id = props.match.params.id
        if(patient_id === 'new') return
        const {data:{data}} = await getPatient(patient_id);
        console.log(data)
        setData(data);
        form.setFieldsValue(data)
      }
    )();
  }, []);

  // useEffect(() => {
  //   console.log("Effect 2");
  // }, [data]);

  const handleChange = ({target}) => {
    console.log(target)
    setData({...data, [target.id]: target.value })
  }

  const handleDataChange = input => {
    console.log(input)
    setData({...data, [input.name]: input.value })
  }

  const handleSubmit = async (e) => {
    // e.preventDefault();
    console.log(data);
    try{
      const patient = await savePatient(data)
      console.log(patient)
      // window.location = "/#/patients"
      setData({})
      form.resetFields();
      console.log("after", data)
    }catch(err){
      if(err.response && err.response.status === 400){
        setErrors({errors: err.response.data.errors});
      }
      else{
        console.error(err.response.data.error)
      }
    }
    
  }
  return (
    <div className="container-fluid">
      <div className="card add_new">
        <div className="card-header ">
          <Link to={`/patients`}><button className="btn " style={{background: '#4288bf', color:'white'}}><i className="fas fa-list pr-2"></i>Patient List</button></Link>
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-md-12">

              <Form
                form={form}
                {...layout}
                // name="basic"
                onFinish = {handleSubmit}
              >
                {/* <Form.Item
                  label="Id"
                  name="id"
                  rules={[
                    {
                      required: true,
                      message: 'Please input your username!',
                    },
                  ]}
                >
                  <Input 
                    name="id"
                    value={data.id || ''}
                    onChange={handleChange} 
                  />
                </Form.Item> */}

                <Form.Item
                  label="Patient Name"
                  name="name"
                  onChange={handleChange} 
                  rules={[
                    {
                      required: true,
                      message: 'Please input your name!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Father Name"
                  name="father_name"
                  
                  onChange={handleChange}
                  rules={[
                    {
                      required: true,
                      message: 'Please input your name!',
                    },
                  ]}
                >
                  <Input  />
                </Form.Item>
                <Form.Item
                  label="Health"
                  name="health"
                  value={data.health || ''}
                  onChange={handleChange}  
                >
                  <Input  />
                </Form.Item>
                <Form.Item
                  label="Color of Skin"
                  name="skin_color"
                  onChange={handleChange}  
                  value={data.skin_color || ''}
                >
                  <Input  />
                </Form.Item>

                <Form.Item
                  label="Profession"
                  name="profession"
                  value={data.profession || ''}
                  onChange={handleChange}  
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Age"
                  name="age"
                  value={data.age || ''}
                  onChange={handleChange}  
                  rules={[
                    {
                      required: true,
                      message: 'Please input your age!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                
                <Form.Item
                  label="Weight"
                  name="weight"
                  value={data.weight || ''}
                  onChange={handleChange} 
                  rules={[
                    {
                      required: true,
                      message: 'Please input your weight!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Gender"
                  name="gender"
                  onChange={e=>handleDataChange({name: 'gender', value: e.target.value})}
                  
                  rules={[
                    {
                      required: true,
                      message: 'Please input your gender!',
                    },
                  ]}
                >
                  <Radio.Group  >
                    <Radio value="Male">Male</Radio>
                    <Radio value="Female">Female</Radio>
                  </Radio.Group>
                </Form.Item>
                <Form.Item
                  label="Address"
                  name="address"
                  value={data.address || ''}
                  onChange={handleChange}  
                  rules={[
                    {
                      required: true,
                      message: 'Please input your address!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
            
                <Form.Item
                  label="Page no"
                  name="page_no"
                  onChange={handleChange}  

                >
                  <Input  />
                </Form.Item>
                <Form.Item
                  label="Register no"
                  name="reg_no"
                  onChange={handleChange}  
                  >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Case no"
                  name="case_no"
                  onChange={handleChange}  
                >
                  <Input />
                </Form.Item>

                {/* <Form.Item
                  label="Date"
                  name="date"
                >
                  <DatePicker 
                    onChange={value=>handleDataChange({name:"date", value})}
                    style={{width: "100%"}}
                    format="DD/MM/YYYY"
                  />
                </Form.Item> */}

                <Form.Item
                  label="How many siblings do you have?"
                  name="sibling"
                  onChange={handleChange}  
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="How many children do you have?"
                  name="children"
                  onChange={handleChange}  
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Is your Fahter alive?"
                  name="is_father_alive"
                  onChange={(e)=> handleDataChange({name: 'is_father_alive', value: e.target.value})} 
                >
                  <Radio.Group >
                    <Radio value="Yes">Yes</Radio>
                    <Radio value="No">No</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  label="Is your Mother alive?"
                  name="is_mother_alive"
                  onChange={(e)=> handleDataChange({name: 'is_mother_alive', value: e.target.value})} 
                >
                  <Radio.Group>
                    <Radio value="Yes">Yes</Radio>
                    <Radio value="No">No</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  label="Is your Husband/Wife alive?"
                  name="is_partner_alive"
                  onChange={(e)=> handleDataChange({name: 'is_partner_alive', value: e.target.value})} 
                >
                  <Radio.Group>
                    <Radio value="Yes">Yes</Radio>
                    <Radio value="No">No</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  label="Do you have any allergy?"
                  name="allergy"
                  onChange={(e)=> handleDataChange({name: 'allergy', value: e.target.value})} 
                >
                  <Radio.Group>
                    <Radio value="Yes">Yes</Radio>
                    <Radio value="No">No</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  label="Do you have blood pressure?"
                  name="blood_pressure"
                  onChange={(e)=> handleDataChange({name: 'blood_pressure', value: e.target.value})} 
                >
                  <Radio.Group>
                    <Radio value="Yes">Yes</Radio>
                    <Radio value="No">No</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  label="Do you sleep properly at night?"
                  name="sleep"
                  onChange={(e)=> handleDataChange({name: 'sleep', value: e.target.value})} 
                >
                  <Radio.Group
                    name="sleep"
                    value={data.sleep || ''}
                    onChange={handleChange} 
                  >
                    <Radio value="Yes">Yes</Radio>
                    <Radio value="No">No</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  label="Does anybody has mental illness in your family?"
                  name="mental_illness"
                  onChange={(e)=> handleDataChange({name: 'mental_illness', value: e.target.value})} 
                >
                  <Radio.Group
                    name="mental_illness"
                    value={data.mental_illness || ''}
                    onChange={handleChange} 
                  >
                    <Radio value="Yes">Yes</Radio>
                    <Radio value="No">No</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  label="Does anybody has cancer in your family?"
                  name="cancer"
                  onChange={(e)=> handleDataChange({name: 'cancer', value: e.target.value})} 
                >
                  <Radio.Group>
                    <Radio value="Yes">Yes</Radio>
                    <Radio value="No">No</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  label="Do you get scared easily?"
                  name="scare"
                  onChange={(e)=> handleDataChange({name: 'scare', value: e.target.value})} 
                >
                  <Radio.Group>
                    <Radio value="Yes">Yes</Radio>
                    <Radio value="No">No</Radio>
                  </Radio.Group>
                </Form.Item>

                  <Form.Item
                  label="Family"
                  name="family"
                  onChange={handleChange}
                >
                  <TextArea 
                    rows={2} 
                  />
                  
                </Form.Item>

                <Form.Item
                  label="Present"
                  name="present"
                  onChange={handleChange}
                >
                  <TextArea 
                    rows={2} 
                  />
                  
                </Form.Item>

                <Form.Item
                  label="Self"
                  name="self"
                  onChange={handleChange}
                >
                  <TextArea 
                    rows={2} 
                  />
                  
                </Form.Item>

                <Form.Item
                  label="Description"
                  name="description"
                  onChange={handleChange}
                >
                  <TextArea 
                    rows={2}
                      
                  />
                  {/* <CKEditor
                    editor = {ClassicEditor}
                    onInit = {editor =>{}
                    }
                    name="description"
                    value={this.state.description}
                    onChange={this.handleCkeditorState}
                    
                  />  */}
                </Form.Item> 

                {/* <Form.Item
                  label="Image"
                  name="image"
                  
                >
                  <Upload
                    name="image"
                  >
                    <Button><UploadOutlined /> Click to Upload</Button>
                  </Upload>
                </Form.Item> */}
                
                <Form.Item {...tailLayout}>
                  <Button  
                    htmlType="submit" 
                    style={{background: '#4288bf', color:'white'}}>
                    Submit
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>

        </div>
      </div>
    </div>
  )
}

export default AddNewPatient;
