import React, { Component } from 'react'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import { Link } from 'react-router-dom';
import { Modal } from 'antd'
import { getPatients,deletePatient, getPatient } from '../../services/patient';




const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };
  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}
export default class Patients extends Component {
  state={
    patients: [
      // {id:1, name: 'mou' , email: 'mou@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:2, name: 'moin' , email: 'moin@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:3, name: 'pantho' , email: 'pantho@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:4, name: 'himel' , email: 'himel@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:5, name: 'parvez' , email: 'parvez@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:6, name: 'supto' , email: 'supto@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:7, name: 'ismity' , email: 'ismity@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:8, name: 'faria' , email: 'faria@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:9, name: 'rafi' , email: 'cap@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:10, name: 'shakal' , email: 'shakal@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
  
  ],
    rowsPerPage:8,
    search:'',
    page:0
  }
  
  async componentDidMount(){
    const {data:{data:patients}} = await getPatients();
    console.log(patients);
    this.setState({patients})
  }

  handleChange = (e) => {
    this.setState({search: e.target.value})
  }

  handleClose=() =>{
    this.setState({data:{}, deleteShow:false, viewShow:false})
  }

  handleViewShow = (data) => {
    this.setState({data, viewShow: true})
   }
 
   handleView = async () => {
     const {data} = this.state
     const patient = await getPatient(data.id)
     this.setState({data: patient.data.data, viewShow:false})
     
     console.log(patient.data.data)
   }

  handleDeleteShow = (data) => {
    this.setState({data, deleteShow: true})
  }


  handleDelete = async(e) => {
    e.preventDefault();
    try{
      const {data} = this.state;
      await deletePatient(data.id);
      console.log(data.id)
      const {data:{data:patients}} = await getPatients()
      // const patients = this.state.patients.filter(item=> item._id !== data._id);
      this.setState({patients, deleteShow: false})
    }catch(err){
      if (err.response && err.response.status === 404) {
        console.error("This record has already been deleted.");
      }
    }
    
  }

  handleChangePage =(e, newPage) => {
    console.log("before",newPage)
    this.setState({page:newPage})
    console.log("after",newPage)
  }
  
  handleChangeRowsPerPage = (e) => {
  
    this.setState({rowsPerPage:e.target.value,page:0})
    
  }
  render() {
    const {patients,page,rowsPerPage,deleteShow, viewShow,data,search} = this.state
    return (
      <div>
        <div className="  ">
          <div className="card-header">
          <span className="na-text" style={{fontSize:'30px'}}>Patients</span>

            <Link to= '/patient/new'><button className="btn btn-sm btn-success float-right"><span className="font-weight-bold">+ Add Patient</span></button></Link>
          </div>
          <div className="">
            <div className="row pr-3">
              <div className="col-md-3 ">
                <input 
                  name="search"
                  value={this.state.search}
                  type="text" 
                  className="form-control my-4 mr-5" 
                  placeholder="Search...." 
                  onChange={this.handleChange}
                />

              </div>
              <div className="col-md-9"></div>

            </div>
            <TableContainer  component={Paper}>
              <Table  aria-label="simple table" size="small" >
                <TableHead  style={{background: '#4288bf '}} >
                  <TableRow>
                    <TableCell className="font-weight-bold text-white" align="left" >Id</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Patient Name</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Father Name</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Address</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Registration no</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Case no</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Page no</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {(rowsPerPage > 0
                      ? patients.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      : patients
                    ).filter(val=>{
                      if(search == '') {
                        return val
                      }else if( val.name.toLowerCase().includes(search.toLowerCase()) || val.father_name.toLowerCase().includes(search.toLowerCase()) || val.reg_no.toLowerCase().includes(search.toLowerCase()) || val.case_no.toLowerCase().includes(search.toLowerCase()) || val.page_no.toLowerCase().includes(search.toLowerCase()) ){
                        return val
                      }
                    }).map((patient,index) => (
                    <TableRow key={index} className="tr">
                      <TableCell >{patient.id} </TableCell>
                      <TableCell >{patient.name} </TableCell>
                      <TableCell align="left">{patient.father_name}</TableCell>
                      <TableCell align="left">{patient.address}</TableCell>
                      <TableCell align="left">{patient.reg_no}</TableCell>
                      <TableCell align="left">{patient.case_no}</TableCell>
                      <TableCell align="left">{patient.page_no}</TableCell>
                      <TableCell align="left">
                        <Link to={`/patientView/${patient.id}`}>
                          <i 
                            className="fa fa-eye mr-3 text-primary" 
                            onClick={()=>this.handleViewShow(patient)}
                          ></i>
                        </Link>
                        <Link to={`/patient/${patient.id}`}>
                          <i 
                            className="fa fa-pencil-square-o text-success mr-3"
                          ></i>
                        </Link>
                        <i 
                          className="fa fa-trash-o clickable text-danger" 
                          onClick={()=>this.handleDeleteShow(patient)} 
                        ></i>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[ 10, 25, { label: 'All', value: -1 }]}
                      colSpan={3}
                      count={patients.length}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      SelectProps={{
                        inputProps: { 'aria-label': 'rows per page' },
                        native: true,
                      }}
                      onChangePage={this.handleChangePage}
                      onChangeRowsPerPage={this.handleChangeRowsPerPage}
                      ActionsComponent={TablePaginationActions}
                    />
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>

            <Modal
              visible={deleteShow}
              onOk = {this.handleDelete}
              onCancel={this.handleClose}
              okText = "Delete"
              okType = 'danger'
            >
              <p>Are you sure you want to <span className="text-danger font-bold">delete</span> this item?</p>
              <div className="text-right">
                {/* <button className="btn btn-sm btn-light mx-3" onClick={this.handleClose}>Cancel</button>
                <button className="btn btn-sm btn-danger" onClick={this.handleDelete}>Delete</button> */}

              </div>
            </Modal>
          </div>
        </div>
      </div>
    )
  }
}
