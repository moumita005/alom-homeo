import React, { Component } from 'react'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import { Link } from 'react-router-dom';
import { Modal } from 'antd'
import { getPrescriptions,deletePrescription, getPrescription } from '../../services/prescription';




const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };
  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}
export default class Prescription extends Component {
  state={
    prescriptions: [
      // {id:1, name: 'mou' , email: 'mou@fdc.com',phone_no: '12113', guardian_phone_no: '345452w' },
      // {id:2, name: 'moin' , email: 'moin@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:3, name: 'pantho' , email: 'pantho@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:4, name: 'himel' , email: 'himel@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:5, name: 'parvez' , email: 'parvez@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:6, name: 'supto' , email: 'supto@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:7, name: 'ismity' , email: 'ismity@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:8, name: 'biproda' , email: 'rafi@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:9, name: 'rafi' , email: 'cap@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
      // {id:10, name: 'shakal' , email: 'shakal@fdc.com',phone_no: '12213', guardian_phone_no: '345452w' },
  
  ],
    rowsPerPage:8,
    search:'',
    page:0,
    deleteShow:false,
    viewShow:false
  }
  async componentDidMount(){
    const {data:{data:prescriptions}} = await getPrescriptions();
    console.log(prescriptions)
    this.setState({prescriptions})

  }
  handleChange = (e) => {
    this.setState({search: e.target.value})
  }

  handleClose=() =>{
    this.setState({data:{}, deleteShow:false, viewShow:false})
  }
  
  handleViewShow = (data) => {
    console.log(data)
    this.setState({data, viewShow: true})
   }
 
  handleView = async () => {
    const {data} = this.state
    console.log(data)
    // const {data:{data:prescription}} = await getPrescription(data.id)
    this.setState({data, viewShow:false})
    
    // console.log(prescription)
  }

  handleDeleteShow = (data) => {
    this.setState({data, deleteShow: true})
  }

  handleDelete = async(e) => {
    e.preventDefault();
    try{
      const {data} = this.state;
      await deletePrescription(data.id)
      console.log(data.id)
      // const {data:{data:prescriptions}} = await getPrescriptions()
      const prescriptions = this.state.prescriptions.filter(item=> item.id !== data.id);
      this.setState({prescriptions, deleteShow: false})
    }catch(err){
      if (err.response && err.response.status === 404) {
        console.error("This record has already been deleted.");
      }
    }
    
  }

  handleChangePage =(e, newPage) => {
    console.log("before",newPage)
    this.setState({page:newPage})
    console.log("after",newPage)
  }
  
  handleChangeRowsPerPage = (e) => {
  
    this.setState({rowsPerPage:e.target.value,page:0})
    
  }
  render() {
    const {prescriptions,page,rowsPerPage,deleteShow, viewShow,data,search} = this.state;
    let medStr = '';
    let medType = '';
    return (
      <div>
        <div className="card ">
          <div className="card-header">
            <span className="na-text" style={{fontSize:'30px'}}>Prescriptions</span>
            <Link to= '/prescription/new'><button className="btn btn-sm btn-success float-right"><span className="font-weight-bold">+ Add Prescription</span></button></Link>
          </div>
          <div className="">
            <div className="row pr-3">
              <div className="col-md-10"></div>
              <div className="col-md-2">
                <input 
                  name="search"
                  value={this.state.search}
                  type="text" 
                  className="form-control my-4" 
                  placeholder="Search...." 
                  onChange={this.handleChange}
                />

              </div>

            </div>
            <TableContainer  component={Paper}>
              <Table  aria-label="simple table" size="small"  >
                <TableHead  style={{background: '#4288bf '}} >
                  <TableRow>
                    <TableCell className="font-weight-bold text-white" align="left">Date</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left">Patient Id</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left">Patient Name</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left">Age</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left">Status</TableCell>
                    {/* <TableCell className="font-weight-bold text-white" align="left">Medicine Type</TableCell> */}
                    {/* <TableCell className="font-weight-bold text-white" style={{border: '1px solid grey'}} align="left">Guardian Phone no</TableCell> */}
                    <TableCell className="font-weight-bold text-white" align="left" >Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody >
                  {(rowsPerPage > 0
                      ? prescriptions.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      : prescriptions
                    ).filter(val=>{
                      if(search == '') {
                        return val
                      }else if(val.patient && val.patient.name.toLowerCase().includes(search.toLowerCase()) ){
                        return val
                      }
                    }).map((prescription,index) => (
                      
                    <TableRow key={index} className="tr">
                      <TableCell  >{prescription.date} </TableCell>
                      <TableCell  >{prescription.patient_id} </TableCell>
                      <TableCell  >{prescription.patient && prescription.patient.name} </TableCell>
                      <TableCell  align="left">{prescription.patient && prescription.patient.age}</TableCell>
                      <TableCell  align="left"><span className="badge badge-warning">{prescription.status}</span></TableCell>
                      {/* {
                        medStr= ''
                      }
                      {
                        medType= ''
                      }
                      {prescription.medicines && prescription.medicines.forEach((med,index)=>{
                        medStr+=med.medicine_name && (index>0?' , ':'' )+med.medicine_name;
                        medType+=med.medicine_type && (index>0?' , ':'' )+med.medicine_type;
                      })}
                      
                      <TableCell  align="left">{medStr}</TableCell>
                      <TableCell  align="left">{medType}</TableCell> */}
                      
                      
                      {/* <TableCell  align="left">{prescription.guardian_phone_no}</TableCell> */}
                      <TableCell align="left">
                        
                          <i 
                            className="fa fa-eye mr-3 text-primary" 
                            onClick={()=>this.handleViewShow(prescription)}
                          ></i>
                        
                        <Link to={`/prescription/${prescription.id}`}>
                          <i 
                            className="fa fa-pencil-square-o text-success mr-3"
                          ></i>
                        </Link>
                        <i 
                          className="fa fa-trash-o clickable text-danger" 
                          onClick={()=>this.handleDeleteShow(prescription)} 
                        ></i>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[ 10, 25, { label: 'All', value: -1 }]}
                      colSpan={3}
                      count={prescriptions.length}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      SelectProps={{
                        inputProps: { 'aria-label': 'rows per page' },
                        native: true,
                      }}
                      onChangePage={this.handleChangePage}
                      onChangeRowsPerPage={this.handleChangeRowsPerPage}
                      ActionsComponent={TablePaginationActions}
                    />
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>
            <Modal
              visible={viewShow}
              onOk= {this.handleView}
              onCancel={this.handleClose}
              width={900}
            >
              <div>
                
                <h5 className="mt-3 text-center lead"></h5>
                <div className="row">
                  <div className="col-md-6">
                    <p>Patient Name: {data.patient && data.patient.name}</p>
                  </div>
                  <div className="col-md-6"></div>
                </div>
                <table  className="mt-2 table table-striped table-bordered table-responsive-sm w-100">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Status</th>
                      
                    </tr>
                </thead>
                <tbody>
                  {data.medicines && data.medicines.map((med,index)=>{
                    return(
                      <tr key={index}>
                        <td>{med.medicine_name}</td>
                        <td>{med.status}</td>
                      </tr> 

                    )
                  })}
                       
                  </tbody>
                </table>
                
              </div>
            </Modal>  
            <Modal
              visible={deleteShow}
              onOk = {this.handleDelete}
              onCancel={this.handleClose}
              okText = "Delete"
              okType = 'danger'
            >
              <p>Are you sure you want to <span className="text-danger font-bold">delete</span> this item?</p>
              <div className="text-right">
                {/* <button className="btn btn-sm btn-light mx-3" onClick={this.handleClose}>Cancel</button>
                <button className="btn btn-sm btn-danger" onClick={this.handleDelete}>Delete</button> */}

              </div>
            </Modal>
          </div>
        </div>
      </div>
    )
  }
}
