import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import { DatePicker, Space } from 'antd';
import moment from 'moment';
import {getPatient, getPatients, savePatient} from '../../services/patient'
import { saveMedicine } from '../../services/medicine';
import { getPrescription, savePrescription } from '../../services/prescription';


export default class AddNewPrescription extends Component {
  state={
    data: {
      medicines: [{}]
    },
    errors:{},
    patients:[],
  }

  async componentDidMount(){
    const prescription_id = this.props.match.params.id
   
    const {data:{data:patients}} = await getPatients();
    console.log(patients)
    this.setState({patients})
    if(prescription_id === 'new') return
    const {data:{data}} = await getPrescription(prescription_id);
    console.log("edit",data)
    this.setState({data})
  }
  
  handleChange = ({target}) => {
    const data = {...this.state.data}
    const errors = {...this.state.errors}
    data[target.name] = target.value;
    if(data.patient_id){
      const patient = this.state.patients.find(patient=> patient.id === data.patient_id)
      console.log( patient.gender)
      data.name = patient.name
      data.gender = patient.gender
      data.weight = patient.weight
      data.age = patient.age
      data.cancer = patient.cancer
      data.blood_pressure = patient.blood_pressure
      data.sleep = patient.sleep
      data.mental_illness = patient.mental_illness
      data.family = patient.family
      data.present = patient.present
      data.self = patient.self
      data.description = patient.description
    }
    delete errors[target.name];
    this.setState({data,errors})
  }

  handleArrayChange= ({target}, arrName, index) => {
    const data = {...this.state.data}
    const arr = data[arrName]
    arr[index][target.name] = target.value
    data[arrName]=arr
    this.setState({data})
  }

  handleDataChange = input => {
    console.log(input)
    this.setState({...this.state.data, [input.name]: input.value })
  }

  addRow = arrName => {
    const data = {...this.state.data}
    data[arrName].push({})
    this.setState({data})
  }
  removeRow = arrName => {
    const data = {...this.state.data}
    data[arrName].pop()
    this.setState({data})
  }

  handleSubmit = async() => {
    const {data} = this.state;
    console.log(data)
    try{
      const {prescription} = await savePrescription(data)
      console.log(prescription)
      
      this.setState({data:{}})

    }catch(err){
      if(err.response && err.response.status === 400){
        this.setState({errors: err.response.data.errors});
      }
      else{
        console.error(err.response.data.error)
      }
    }
  }
  
  render() {
    const {data,patients} = this.state
    return (
      <div className="container-fluid">
        <div className="card ">
          <div className="card-header ">
            <Link to={`/prescriptions`}><button className="btn btn-sm " style={{background: '#4288bf', color:'white'}}><i className="fas fa-list pr-2"></i>Prescription List</button></Link>
            <Link to={`/patient/:id`}><button className="btn btn-sm float-right" style={{background: '#028a0f', color:'white'}}><span className="font-weight-bold">+ Add Patient</span></button></Link>
          </div>
          <div className="card-body">
            <p className="h2 text-center" style={{color:'#4288ab'}}> Prescription </p>
            <div className="row">
              <div className="col-md-12 p-5">
                <div  noValidate autoComplete="off">
                  <div className="row p-2">
                    <div className="col-md-6">
                      
                      {/* <TextField 
                        name="patient_id"
                        value={data.patient_id || ''}
                        onChange={this.handleChange}
                        id="standard-full-width" 
                        fullWidth 
                        label="Patient ID" 
                        InputLabelProps={{
                          shrink: true,
                        }} 
                        margin="normal"
                      /> */}
                      <InputLabel shrink id="demo-simple-select-placeholder-label-label">
                        Patient ID
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-placeholder-label-label"
                        id="demo-simple-select-placeholder-label"
                        name="patient_id"
                        value={data.patient_id || ''}
                        onChange={this.handleChange}
                        displayEmpty
                        margin="normal"
                        fullWidth
                      > 
                        
                        {
                          patients.map(item=>{
                            return(
                              <MenuItem value={item.id}>{`${item.id}-${item.name}-${item.case_no}`}</MenuItem>
                            )
                          })
                        }
                        
                        
                      </Select>
                      {/* <Autocomplete
                        id="combo-box-demo"
                        options={patients}
                        
                        getOptionLabel={patients.map((patient) => patient.id)}
                        // style={{ width: 300 }}
                        renderInput={(params) => <TextField {...params} label="Combo box" variant="outlined" />}
                      /> */}
                      
                      
                    </div>
                    <div className="col-md-6">
                    <div className="">Date </div>
                      <DatePicker 
                        value={data.date && moment(data.date, 'DD/MM/YYYY')}
                        format={"DD/MM/YYYY"} 
                        allowClear={true}
                        onChange={(date, dateString)=>{
                          data["date"] = date;
                          this.setState({ data })
                        }}
                        style={{width: "100%"}}
                      />

                      {/* <DatePicker 
                        onChange={value=>this.handleDataChange({name:"date", value})}
                        style={{width: "100%"}}
                        format="DD/MM/YYYY"
                      /> */}
                    </div>
                  </div>  
                  <div className="row">
                    <div className="col-md-12">
                       <div className="card mt-5 p-5">
                         <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                                <TextField 
                                  name="name"
                                  value={data.name ||  ''}
                                  // onChange={this.handleChange}
                                  id="standard-full-width" 
                                  fullWidth 
                                  label="Patient Name" 
                                  InputLabelProps={{
                                    shrink: true,
                                  }} 
                                  margin="normal"
                                  readOnly
                                />
                                <TextField 
                                  name="weight"
                                  value={data.weight || ''}
                                  // onChange={this.handleChange}
                                  id="standard-full-width" 
                                  fullWidth 
                                  label="weight" 
                                  InputLabelProps={{
                                    shrink: true,
                                  }} 
                                  margin="normal"
                                  readOnly
                                />
                            </div>
                            <div className="col-md-6">
                              <TextField 
                                name="gender"
                                value={data.gender || ''}
                                // onChange={this.handleChange}
                                id="standard-full-width" 
                                fullWidth 
                                label="Gender" 
                                InputLabelProps={{
                                  shrink: true,
                                }} 
                                margin="normal"
                                readOnly
                              />
                              <TextField 
                                name="age"
                                value={data.age || ''}
                                // onChange={this.handleChange}
                                id="standard-full-width" 
                                fullWidth 
                                label="Age" 
                                InputLabelProps={{
                                  shrink: true,
                                }} 
                                margin="normal"
                                readOnly
                              />
                            </div>
                          </div>
                         </div>
                       </div>
                    </div>
                  </div>
                  <div className="accordion mt-5" id="accordionExample">
                    <div className="accordion-item">
                      <h2 className="accordion-header" id="headingOne">
                        <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          <h5 className="text-center " style={{color:'#4288ab'}}>Case Studies</h5>
                        </button>
                      </h2>
                      <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                          <div className="row">
                            
                            <div className="col-md-3">
                              {/* <FormControl component="fieldset" >
                                <FormLabel component="legend" style={{fontSize:'14px'}}>Do you have any allergy?</FormLabel>
                                <RadioGroup 
                                  aria-label="allergy" 
                                  name="allergy"
                                  value={data.allergy || ''}
                                  onChange={this.handleChange}
                                >
                                  <FormControlLabel value="yes" control={<Radio />} label="Yes" />
                                  <FormControlLabel value="no"  control={<Radio />} label="No" />
                                </RadioGroup>
                              </FormControl> */}

                              <FormControl component="fieldset" >
                                <FormLabel component="legend" style={{fontSize:'14px'}}>Does anybody has cancer in your family?</FormLabel>
                                <RadioGroup 
                                  aria-label="cancer" 
                                  name="cancer"
                                  value={data.cancer || ''}
                                  readOnly
                                  // onChange={this.handleChange}
                                >
                                  <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                  <FormControlLabel value="No"  control={<Radio />} label="No" />
                                </RadioGroup>
                              </FormControl>
                              
                            </div>
                            <div className="col-md-3">
                              <FormControl component="fieldset" >
                                <FormLabel component="legend" style={{fontSize:'14px'}}>Do you have blood pressure?</FormLabel>
                                <RadioGroup 
                                  aria-label="pressure" 
                                  name="blood_pressure"
                                  value={data.blood_pressure || ''}
                                  readOnly
                                  // onChange={this.handleChange}
                                >
                                  <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                  <FormControlLabel value="No"  control={<Radio />} label="No" />
                                </RadioGroup>
                              </FormControl>
                              {/* <FormControl component="fieldset" >
                                <FormLabel component="legend" style={{fontSize:'14px'}}>Do you get scared easily?</FormLabel>
                                <RadioGroup 
                                  aria-label="scare" 
                                  name="scare"
                                  value={data.scare || ''}
                                  onChange={this.handleChange}
                                >
                                  <FormControlLabel value="yes" control={<Radio />} label="Yes" />
                                  <FormControlLabel value="no"  control={<Radio />} label="No" />
                                </RadioGroup>
                              </FormControl> */}
                            </div>
                            <div className="col-md-3">
                              <FormControl component="fieldset" >
                                <FormLabel component="legend" style={{fontSize:'14px'}}>Do you sleep properly at night?</FormLabel>
                                <RadioGroup 
                                  aria-label="sleep" 
                                  name="sleep"
                                  value={data.sleep || ''}
                                  readOnly
                                  // onChange={this.handleChange}
                                >
                                  <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                  <FormControlLabel value="No"  control={<Radio />} label="No" />
                                </RadioGroup>
                              </FormControl>
                            </div>
                            <div className="col-md-3">
                              <FormControl component="fieldset" >
                                <FormLabel component="legend" style={{fontSize:'14px'}}>Does anybody has mental illness in your family?</FormLabel>
                                <RadioGroup 
                                  aria-label="mental_illness" 
                                  name="mental_illness"
                                  value={data.mental_illness || ''}
                                  readOnly
                                  // onChange={this.handleChange}
                                >
                                  <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                  <FormControlLabel value="No"  control={<Radio />} label="No" />
                                </RadioGroup>
                              </FormControl>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-12">
                              <div className="form-floating mt-5">
                                <textarea 
                                  name="family"
                                  value={data.family || ''}
                                  // onChange={this.handleChange}
                                  className="form-control" 
                                  id="floatingTextarea2" 
                                  rows="4" 
                                  style={{height: '100%'}}
                                  readOnly
                                ></textarea>
                                <label for="floatingTextarea2">Family</label>
                              </div>
                              <div className="form-floating my-5">
                                <textarea 
                                  name="present"
                                  value={data.present || ''}
                                  // onChange={this.handleChange}
                                  className="form-control" 
                                  id="floatingTextarea2" 
                                  rows="4" 
                                  style={{height: '100%'}}
                                  readOnly
                                ></textarea>
                                <label for="floatingTextarea2">Present</label>
                              </div>
                              <div className="form-floating my-5">
                                <textarea 
                                  name="self"
                                  value={data.self || ''}
                                  // onChange={this.handleChange}
                                  className="form-control" 
                                  id="floatingTextarea2" 
                                  rows="4" 
                                  style={{height: '100%'}}
                                  readOnly
                                ></textarea>
                                <label for="floatingTextarea2">Self</label>
                              </div>
                              <div className="form-floating my-5">
                                <textarea 
                                  name="description"
                                  value={data.description || ''}
                                  // onChange={this.handleChange}
                                  className="form-control" 
                                  id="floatingTextarea2" 
                                  rows="4" 
                                  style={{height: '100%'}}
                                  readOnly
                                ></textarea>
                                <label for="floatingTextarea2">Description</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  
                  <div className="table-responsive">
                    <table className="table table-striped table-hover mt-5">
                      <thead style={{background: '#4288bf', color:'white'}}>
                        <tr className="text-center">
                          <th>Medicine Name</th>
                          <th>Medicine Type</th>
                          <th>Days</th>
                          <th>Instruction</th>
                          <th>Remove</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          data.medicines && data.medicines.map((item,index)=>{
                            return(
                              <tr className="table-active" key={index}>
                                <td>
                                  <input
                                    name="medicine_name"
                                    value={item.medicine_name || ''}
                                    onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                                    className="form-control" 
                                  />
                                </td>
                                <td>
                                  <input
                                    name="medicine_type"
                                    value={item.medicine_type || ''}
                                    onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                                    className="form-control" 
                                  />
                                </td>
                                <td>
                                  <input
                                    name="day"
                                    value={item.day || ''}
                                    onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                                    className="form-control" 
                                  />
                                </td>
                                <td>
                                  <textarea 
                                    name="instruction"
                                    value={item.instruction || ''}
                                    onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                                    className="form-control" 
                                    rows="2"
                                  ></textarea>
                                </td>
                                
                                <td>
                                  <div className="text-center">
                                    
                                    <button className="btn btn-sm btn-danger" onClick={()=>this.removeRow('medicines')}>Remove</button>
                                  </div>
                                </td>
                              </tr>
                            )
                          })
                        }
                        
                      </tbody>
                      <button className="btn btn-sm btn-success mt-2 " onClick={()=>this.addRow('medicines')}>Add</button>
                    </table>  

                  </div>          
                </div>
              </div>
            </div>
            <div className="text-center">
              <button 
                type="submit"
                className="btn btn-sm " 
                style={{background: '#4288bf', color:'white'}}
                onClick={this.handleSubmit}
              >Submit</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
