import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import { Modal , Tag } from 'antd'
import { deletePrescription, getPrescription, getPrescriptions,savePrescription } from '../../services/prescription';
import { getMedicine, saveMedicine } from '../../services/medicine';
import moment from 'moment';




const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };
  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}
export default class Prescriptions extends Component {
  state={
    data: {},
    prescriptions: [
      // {id:1, name: 'mou' , status: 'pending', medicine_name: 'napa', day: 7},
      // {id:2, name: 'moin' , status: 'pending',medicine_name: 'napa', day: 7},
      // {id:3, name: 'pantho' ,status: 'pending' ,medicine_name: 'napa', day: 7},
      // {id:4, name: 'himel' ,status: 'pending' ,medicine_name: 'napa', day: 7},
      // {id:5, name: 'parvez' ,status: 'pending',medicine_name: 'napa', day: 7 },
      // {id:6, name: 'supto' , status: 'pending', medicine_name: 'napa', day: 7},
      // {id:7, name: 'ismity' ,status: 'pending' , medicine_name: 'napa', day: 7},
      // {id:8, name: 'faria' ,status: 'pending', medicine_name: 'napa', day: 7 },
      // {id:9, name: 'rafi' , status: 'pending', medicine_name: 'napa', day: 7 },
      // {id:10, name: 'shakal' ,status: 'pending', medicine_name: 'napa', day: 7 },
  
  ],
    rowsPerPage:8,
    search:'',
    page:0,
    viewShow: false,
    deleteShow: false,
    editShow: false
  }
  
  async componentDidMount(){
    const {data:{data:prescriptions}} = await getPrescriptions();
    // console.log(prescriptions);
    this.setState({ prescriptions })
  }

  handleChange = (e) => {
    this.setState({search: e.target.value})
  }

  handleChangePage =(e, newPage) => {
    // console.log("before",newPage)
    this.setState({page:newPage})
    // console.log("after",newPage)
  }
  
  handleChangeRowsPerPage = (e) => {
  
    this.setState({rowsPerPage:e.target.value,page:0})
    
  }

  handleViewShow = (data) => {
    this.setState({data, viewShow: true})
  }
 
  handleView = async () => {
     const {data} = this.state
     console.log(data);
     this.setState({viewShow:false})
    //  this.setState({data: payment.data.data, viewShow:false})
     
    //  console.log(payment.data.data)
  }

  handleEditShow = (data) => {
    this.setState({data, editShow: true})
  }

  addRow = arrName => {
    const data = {...this.state.data}
    data[arrName].push({})
    this.setState({data})
  }
  removeRow = arrName => {
    const data = {...this.state.data}
    data[arrName].pop()
    this.setState({data})
  }

  handleDeleteShow = (data) =>{
    this.setState({data, deleteShow: true})
  }

  handleDelete = async(e) => {
    e.preventDefault();
    const {data} = this.state;
    console.log(data.id)
    await deletePrescription(data.id);
    const {data:{data:prescriptions}} = await getPrescriptions();
    console.log(prescriptions);
    this.setState({prescriptions,deleteShow:false})
  }
  
  handleClose=() =>{
    this.setState({data:{},viewShow:false, deleteShow:false, editShow:false })
  }

  handleArrayChange= ({target}, arrName, index) => {
    const data = {...this.state.data}
    const arr = data[arrName]
    arr[index][target.name] = target.value
    data[arrName]=arr
    this.setState({data})
  }


  handleSubmit = async() => {
    const {data} = this.state;
    console.log(data)
    try{
      await savePrescription(data)
      
      const {data:prescription} = await getPrescription(data.id);
      console.log(prescription.data);
      this.setState({data:prescription.data, editShow:false})
    }catch(err){
      if(err.response && err.response.status === 400){
        this.setState({errors: err.response.data.errors});
      }
      else{
        console.error(err.response.data.error)
      }
    }
  }

  render() {
    const {prescriptions,page,rowsPerPage,deleteShow, viewShow,data,search,editShow} = this.state
    return (
      <div>
        <div className="">
          <div className="card-header">
            <span className="na-text" style={{fontSize:'30px'}}>Prescriptions</span>
            <Link to= '/prescription/new'><button className="btn btn-sm btn-success float-right"><span className="font-weight-bold">+ Add Prescription</span></button></Link>
          </div>
          <div className="">
            <div className="row">
              <div className="col-md-3">
                <input 
                  name="search"
                  value={search}
                  type="text" 
                  className="form-control my-4 " 
                  placeholder="Search...." 
                  onChange={this.handleChange}
                />

              </div>
              <div className="col-md-9"></div>

            </div>
            <TableContainer  component={Paper}>
              <Table  aria-label="simple table" size="small" >
                <TableHead  style={{background: '#4288bf '}} >
                  <TableRow>
                    <TableCell className="font-weight-bold text-white" align="left" >Date</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Patient Id</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Patient Name</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" >Age</TableCell>
                    <TableCell className="font-weight-bold text-white" align="center">Status</TableCell>
                    {/* <TableCell className="font-weight-bold text-white" align="left" style={{border: '1px solid black'}}>Phone no</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" style={{border: '1px solid black'}}>Guardian Phone no</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" style={{border: '1px solid black'}}>Educational Qualification</TableCell>
                    <TableCell className="font-weight-bold text-white" align="left" style={{border: '1px solid black'}}>Local Guardian Phone</TableCell> */}
                    <TableCell className="font-weight-bold text-white" align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody >
                  {(rowsPerPage > 0
                      ? prescriptions.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      : prescriptions
                    ).filter(val=>{
                      if(search == '') {
                        return val
                      }else if( val.patient.name.toLowerCase().includes(search.toLowerCase()) || val.date.toLowerCase().includes(search.toLowerCase())  ){
                        return val
                      }
                    }).map((prescription,index) => (
                    <TableRow key={index} className="tr">
                      <TableCell align="left">{prescription.date && moment(prescription.date).format("DD/MM/YYYY")} </TableCell>
                      <TableCell align="left">{prescription.patient && prescription.patient.id} </TableCell>
                      <TableCell align="left">{prescription.patient && prescription.patient.name} </TableCell>
                      <TableCell align="left">{prescription.patient && prescription.patient.age} </TableCell>
                      <TableCell align="center">{prescription.status === 'Pending'? (
                        <Tag color="gold" style={{fontSize: '12px', fontWeight: '900'}}>{prescription.status}</Tag>
                        ) : (
                        <Tag color="geekblue" style={{fontSize: '12px', fontWeight: '900'}}>{prescription.status}</Tag>
                        ) }
                      </TableCell>
                      {/* <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.email}</TableCell>
                      <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.phone_no}</TableCell>
                      <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.guardian_phone_no}</TableCell>
                      <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.educational_qualification}</TableCell>
                      <TableCell style={{border: '1px solid black'}} align="left">{pharmacy.local_guardian_phone_no}</TableCell> */}
                      <TableCell align="center"  >
                        {/* <Link to={`/studentView/${student._id}`}> */}
                        <i 
                          className="fa fa-eye mr-3 text-primary" 
                          onClick = {()=> {
                            return(this. handleViewShow(prescription))}}
                         
                        ></i>
                        {/* {
                          console.log(prescription.status)
                        } */}
                        {
                          prescription.status==='Pending' ? (<button className="btn"><i 
                            className="fa fa-pencil-square-o text-success mr-3"
                            onClick={()=>this.handleEditShow(prescription)}
                            // disabled={this.input.value?"true":""}
                          ></i></button>): (<button className="btn" disabled><i 
                            className="fa fa-pencil-square-o text-success mr-3"
                            onClick={()=>this.handleEditShow(prescription)}
                            // disabled={this.input.value?"true":""}
                          ></i></button>)
                          // <Link to={`/prescription/${prescription.id}`}>
                                                              
                          //                                   </Link> 
                        }
                        
                        {/* </Link> */}
                        {/* <Link to={`/student/${student._id}`}>
                          <i 
                            className="fa fa-pencil-square-o text-success mr-3"
                          ></i>
                        </Link> */}
                        
                        <i 
                          className="fa fa-trash-o clickable text-danger" 
                          onClick={() => this.handleDeleteShow(prescription)} 
                        ></i>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[ 10, 25, { label: 'All', value: -1 }]}
                      colSpan={3}
                      count={prescriptions.length}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      SelectProps={{
                        inputProps: { 'aria-label': 'rows per page' },
                        native: true,
                      }}
                      onChangePage={this.handleChangePage}
                      onChangeRowsPerPage={this.handleChangeRowsPerPage}
                      ActionsComponent={TablePaginationActions}
                    />
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>
            <Modal
              visible={viewShow}
              onOk= {this.handleView}
              onCancel={this.handleClose}
              width={1600}
            >
              <div>
                
                <h5 className="mt-3 text-center lead"></h5>
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      {/* <div className="card-header" style={{background: '#4288bf '}}>
                        <div className="h5"><span style={{borderBottom: '1px solid white' , color: 'white'}}>Patient Info</span></div>
                      </div> */}
                      <div  style={{background: '#4288bf '}}>
                        <p style={{fontSize: '16px'}} className="pt-3 pl-3 text-white"><span className="font-weight-bold"> Name: </span>{data.patient && data.patient.name}, 
                        <span className="font-weight-bold pl-3"> Age: </span>{data.patient && data.patient.age},
                        <span className="font-weight-bold pl-3"> Weight: </span>{data.patient && data.patient.weight},
                        <span className="font-weight-bold pl-3"> Gender: </span>{data.patient && data.patient.gender},
                        <span className="font-weight-bold pl-3"> Case No: </span>{data.patient && data.patient.case_no},
                        <span className="font-weight-bold pl-3"> Registration No: </span>{data.patient && data.patient.reg_no}</p>
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
                <div className="h5 mt-3 text-center"><span style={{borderBottom: '1px solid black'}}>Medicine</span></div>
                <table  className="mt-2 table  table-hover table-sm table-responsive-sm w-100">
                  <thead  className="" style={{background: '#D9EDF7 ', color: '#3C4767'}}>
                    <tr>
                      <th> Name</th>
                      <th> Type</th>
                      <th> Days</th>
                      <th> Instructions</th>
                      
                    </tr>
                </thead>
                <tbody>
                  {data.medicines && data.medicines.map((med,index)=>{
                    return(
                      <tr key={index}>
                        <td>{med.medicine_name}</td>
                        <td>{med.medicine_type}</td>
                        <td>{med.day}</td>
                        <td>{med.instruction}</td>
                      </tr> 

                    )
                  })}
                       
                  </tbody>
                </table>
                
              </div>
            </Modal> 
            <Modal
              visible={deleteShow}
              onOk = {this.handleDelete}
              onCancel={this.handleClose}
              okText = "Delete"
              okType = 'danger'
            >
              <p>Are you sure you want to <span className="text-danger font-bold">delete</span> this item?</p>
              <div className="text-right">
                {/* <button className="btn btn-sm btn-light mx-3" onClick={this.handleClose}>Cancel</button>
                <button className="btn btn-sm btn-danger" onClick={this.handleDelete}>Delete</button> */}

              </div>
            </Modal>
            <Modal
              visible={editShow}
              onOk = {this.handleSubmit}
              onCancel={this.handleClose}
              okText = "Submit"
              width={1200}
              // okType = 'danger'
            >
              <div>
              <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      {/* <div className="card-header" style={{background: '#4288bf '}}>
                        <div className="h5"><span style={{borderBottom: '1px solid white' , color: 'white'}}>Patient Info</span></div>
                      </div> */}
                      <div  style={{background: '#4288bf '}}>
                        <p style={{fontSize: '16px'}} className="pt-3 pl-3 text-white"><span className="font-weight-bold"> Name: </span>{data.patient && data.patient.name}, 
                        <span className="font-weight-bold pl-3"> Age: </span>{data.patient && data.patient.age},
                        <span className="font-weight-bold pl-3"> Weight: </span>{data.patient && data.patient.weight},
                        <span className="font-weight-bold pl-3"> Gender: </span>{data.patient && data.patient.gender},
                        <span className="font-weight-bold pl-3"> Case No: </span>{data.patient && data.patient.case_no},
                        <span className="font-weight-bold pl-3"> Registration No: </span>{data.patient && data.patient.reg_no}</p>
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
              <div className="table-responsive">
                    <table className="table table-striped table-hover mt-5">
                      <thead style={{background: '#4288bf', color:'white'}}>
                        <tr className="text-center">
                          <th>Medicine Name</th>
                          <th>Medicine Type</th>
                          <th>Days</th>
                          <th>Instruction</th>
                          <th>Remove</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          data.medicines && data.medicines.map((item,index)=>{
                            return(
                              <tr className="table-active" key={index}>
                                <td>
                                  <input
                                    name="medicine_name"
                                    value={item.medicine_name}
                                    onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                                    className="form-control" 
                                  />
                                </td>
                                <td>
                                  <input
                                    name="medicine_type"
                                    value={item.medicine_type}
                                    onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                                    className="form-control" 
                                  />
                                </td>
                                <td>
                                  <input
                                    name="day"
                                    value={item.day}
                                    onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                                    className="form-control" 
                                  />
                                </td>
                                <td>
                                  <textarea 
                                    name="instruction"
                                    value={item.instruction}
                                    onChange={(e)=>this.handleArrayChange(e,'medicines', index)}
                                    className="form-control" 
                                    rows="2"
                                  ></textarea>
                                </td>
                                
                                <td>
                                  <div className="text-center">
                                    
                                    <button className="btn btn-sm btn-danger" onClick={()=>this.removeRow('medicines')}>Remove</button>
                                  </div>
                                </td>
                              </tr>
                            )
                          })
                        }
                        
                      </tbody>
                      <button className="btn btn-sm btn-success mt-2 " onClick={()=>this.addRow('medicines')}>Add</button>
                    </table>  

                  </div>   
              </div>
            </Modal>
          </div>
        </div>
      </div>
    )
  }
}
