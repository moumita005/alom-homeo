import {
  AppstoreOutlined,
  BarChartOutlined,
  CloudOutlined,
  ShopOutlined,
  TeamOutlined,
  UserOutlined,
  UploadOutlined,
  VideoCameraOutlined,
  MenuUnfoldOutlined,
  MedicineBoxOutlined,
  DashboardOutlined
} from '@ant-design/icons';

export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: <DashboardOutlined />
    },
    {
      title: "Patient",
      icon:<UserOutlined/>,
      children: [
        {
          name: "Add Patient",
          url: "/patient/new",
        },
        {
          name: "Patient List",
          url: "/patients",
        },
        // {
        //   name: "Add Document",
        //   url: "/patient/document/new"
        // },
        // {
        //   name: "Documents",
        //   url: "/documents"
        // }
      ]
    },
    // {
    //   title: "Appointment",
    //   icon: <UserOutlined/>,
    //   children: [
    //     {
    //       name: "Add Appointment",
    //       url: "/appointment/new",
    //       icon: "far fa-user"
    //     },
    //     {
    //       name: "Appointments",
    //       url: "/appointments",
    //       icon: "fas fa-user-friends"
    //     }
    //   ]
    // },
    {
      title: "Prescription",
      icon:<TeamOutlined/>,
      children: [
        {
          name: "Add Prescription",
          url: "/prescription/new",
          icon: "far fa-user",
        },
        {
          name: "Prescriptions",
          url: "/prescriptions",
          icon: "fas fa-user-friends"
        }
      ]
    },
    {
      name: "Pharmacy",
      url: "/pharmacy",
      icon: <MedicineBoxOutlined />
    },
    
  ]
}