
import React from "react";

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const AddPatient = React.lazy(() => import('./views/patient/AddNew'));
const Patients = React.lazy(() => import('./views/patient/Patients'));
const AddDocument = React.lazy(() => import('./views/patient/AddNewDocument'));
const Documents = React.lazy(() => import('./views/patient/Documents'));
const Prescriptions = React.lazy(() => import('./views/Doctor/Prescriptions'));
const AddPrescription = React.lazy(() => import('./views/Doctor/AddNewPrescription'));
const Pharmacy = React.lazy(() => import('./views/pharmacy/Pharmacy'));


const routes = [
  { path: "/dashboard", exact: true, name: "Dashboard", component: Dashboard },
  { path: "/patient/:id", exact: true, name: "Patient", component: AddPatient },
  { path: "/patients", exact: true, name: "Patients", component: Patients },
  { path: "/patient/document/:id", exact: true, name: "Patient", component: AddDocument },
  { path: "/documents", exact: true, name: "Patients", component: Documents },
  { path: "/prescriptions", exact: true, name: 'Doctor', component: Prescriptions},
  { path: "/prescription/:id", exact: true, name: "Doctor", component: AddPrescription },
  { path: "/pharmacy", exact: true, name: "Doctor", component: Pharmacy },
]


export default routes;