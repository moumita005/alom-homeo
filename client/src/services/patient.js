import http from './http';


const apiUrl = '/api/patients';


export function getPatients(){
  return http.get(apiUrl);
}

export function getPatient(id){
  return http.get(`${apiUrl}/${id}`);
}

export function savePatient(data){
  if(data.id) return http.put(`${apiUrl}/${data.id}`,data)
    return http.post(apiUrl, data)
  
}

export function deletePatient(id){
  return http.delete(`${apiUrl}/${id}`)
}