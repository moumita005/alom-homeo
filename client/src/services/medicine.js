import http from './http';


const apiUrl = '/api/medicines';


export function getMedicines(){
  return http.get(apiUrl)
}

export function getMedicine(id){
  return http.get(`${apiUrl}/${id}`)
}


export function saveMedicine(data){
  if(data.id) return http.put(`${apiUrl}/${data.id}`,data);
  return http.post(apiUrl, data)
}

export function deleteMedicine(id){
  return http.delete(`${apiUrl}/${id}`)
}