import http from './http';
import jwtDecode from 'jwt-decode';
// import {toast} from 'react-toastify'

const apiUrl = '/api/auth';
const tokenKey = "token";


setTimeout(() => {
  http.setJWT(getJWT());
}, 1000);

export async function register(user) {
  const { name, email, password, phone } = user;
  const {data} = await http.post(`${apiUrl}/register`, { name, email, password, phone } );
  console.log(data)
  localStorage.setItem(tokenKey, data.token);
}

export async function login(user) {
  const { email, password } = user;
  
  const {data} = await http.post(`${apiUrl}/login`, { email, password } );
  localStorage.setItem(tokenKey, data.token);
}

export function getCurrentUser(){
  try{
    const token = localStorage.getItem(tokenKey);
    return jwtDecode(token)

  }catch (error) {
    return null;
  }
}

export function getCurrentUserProfile(){
  return http.get(`${apiUrl}/me`);
}

export function loginWithJWT(token) {
  localStorage.setItem(tokenKey, token);
}

export function logout() {
  localStorage.clear();
}

export function getJWT() {
  return localStorage.getItem(tokenKey);
}


export default {
  register, login, loginWithJWT, logout, getCurrentUser, getCurrentUserProfile, getJWT
}