import http from './http'

const apiUrl = '/api/prescriptions';


export function getPrescriptions(){
  return http.get(apiUrl);
}


export function getPrescription(id){
  return http.get(`${apiUrl}/${id}`);
}


export function savePrescription(data){
  if(data.id) return http.put(`${apiUrl}/${data.id}`, data)
  return http.post(apiUrl, data)
}


export function deletePrescription(id){
  return http.delete(`${apiUrl}/${id}`)
}