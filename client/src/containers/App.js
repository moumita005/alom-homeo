import React from 'react'
import './App.css';
import 'antd/dist/antd.css'
import { HashRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'

const loading = () => <div className="pt-3 text-center">Loading...</div>

const DefaultLayout = React.lazy(()=> import('./containers/DefaultLayout'))
const Page500 = React.lazy(() => import('./views/Pages/Page500/Page500'))
const Page404 = React.lazy(()=> import('./views/Pages/Page404/Page404'))
const Login = React.lazy(() => import('./views/Pages/Login/Login'))
const Register = React.lazy(() => import('./views/Pages/Register/Register'))



function App() {
  return (
    <HashRouter>
     <React.Suspense fallback={loading()} >
       <Switch>
         <Route exact path="/500" name="Page 500" render={props => <Page500 {...props} />} />
         <Route exact path="/404" name="Page 404" render={props => <Page404 {...props} /> } />
         <Route exact path="/login" name="Login" render={props => <Login {...props} /> } />
         <Route exact path="/register" name="register" render={props => <Register {...props} /> } />

         <Route path="/" name="Home" render={props => <DefaultLayout {...props} />} />
       </Switch>
     </React.Suspense>
    </HashRouter>
  );
}

export default App;
