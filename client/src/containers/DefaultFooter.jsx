import React, { Component } from 'react';
import {Layout} from 'antd';

const {Footer} = Layout

export default class DefaultFooter extends Component {
  render() {
    return (
      <div>
        <Footer style={{ textAlign: 'center' }}>Developed by <a target="_blank" href="https://semicolonit.com">Semicolon IT Solutions</a></Footer>
      </div>
    )
  }
}
