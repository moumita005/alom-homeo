-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2021 at 05:40 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alom_homeo`
--

-- --------------------------------------------------------

--
-- Table structure for table `medicines`
--

CREATE TABLE `medicines` (
  `id` int(11) NOT NULL,
  `medicine_name` varchar(255) NOT NULL,
  `medicine_type` varchar(255) NOT NULL,
  `day` varchar(255) NOT NULL,
  `instruction` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `unit_price` varchar(255) NOT NULL,
  `prescription_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `medicines`
--

INSERT INTO `medicines` (`id`, `medicine_name`, `medicine_type`, `day`, `instruction`, `quantity`, `unit_price`, `prescription_id`, `created_at`, `updated_at`) VALUES
(61, 'docopa', 'antibiotic', '14days', '1+0+1', '10', '31', 39, '2021-03-08 03:53:51', '2021-03-08 03:54:54'),
(62, 'fexo', 'antibiotic', '14days', '1+1+0', '15', '5', 39, '2021-03-08 03:53:51', '2021-03-08 03:54:54'),
(63, 'docopa', 'antibiotic', '14days', '1+0+1', '8', '12', 40, '2021-03-08 03:59:52', '2021-03-08 04:08:38'),
(64, 'paracetamol', 'fever', '15days', '1+0+1', '10', '1', 40, '2021-03-08 03:59:52', '2021-03-08 04:08:38'),
(65, 'nux', 'allery', '14', '1+0+1', '12', '2', 41, '2021-03-08 04:11:30', '2021-03-08 04:12:58'),
(66, 'fexo', 'allergy', '10', '1+1+0', '10', '1', 41, '2021-03-08 04:11:30', '2021-03-08 04:12:58'),
(67, 'docopa', 'antibiotic', '14days', '', '3', '12', 42, '2021-03-08 04:28:07', '2021-03-08 04:29:43'),
(68, 'fexo', 'antibiotic', '14days', '', '8', '5', 42, '2021-03-08 04:28:07', '2021-03-08 04:29:43'),
(69, 'copa', 'antibiotic', '14days', '', '3', '21', 42, '2021-03-08 04:28:07', '2021-03-08 04:29:43'),
(70, 'docopa', 'antibiotic', '14days', '1+0+1', '8', '12', 43, '2021-03-08 04:34:41', '2021-03-08 04:38:21'),
(71, 'fexo', 'allergy', '14days', '1+1+0', '10', '5', 43, '2021-03-08 04:34:41', '2021-03-08 04:38:21');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `health` varchar(255) NOT NULL,
  `skin_color` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `weight` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `page_no` varchar(255) NOT NULL,
  `reg_no` varchar(255) NOT NULL,
  `case_no` varchar(255) NOT NULL,
  `sibling` varchar(255) NOT NULL,
  `children` varchar(255) NOT NULL,
  `is_father_alive` varchar(255) NOT NULL,
  `is_mother_alive` varchar(255) NOT NULL,
  `is_partner_alive` varchar(255) NOT NULL,
  `allergy` varchar(255) NOT NULL,
  `scare` varchar(255) NOT NULL,
  `cancer` varchar(255) NOT NULL,
  `blood_pressure` varchar(255) NOT NULL,
  `sleep` varchar(255) NOT NULL,
  `mental_illness` varchar(255) NOT NULL,
  `family` varchar(255) NOT NULL,
  `present` varchar(255) NOT NULL,
  `self` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `name`, `father_name`, `health`, `skin_color`, `profession`, `age`, `weight`, `gender`, `address`, `page_no`, `reg_no`, `case_no`, `sibling`, `children`, `is_father_alive`, `is_mother_alive`, `is_partner_alive`, `allergy`, `scare`, `cancer`, `blood_pressure`, `sleep`, `mental_illness`, `family`, `present`, `self`, `description`, `date`, `created_at`, `updated_at`) VALUES
(10, 'mou', 'Uttam Kumar Baral', 'good', 'white', 'Software developer', '23', '65', 'Female', 'ranarOffice', '12', '2', '2', '1', '0', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'xdfbhst', 'stghbstg', 'hbgbh', 'tghbdftgb', '2021-02-27 11:22:33', '2021-02-27 11:22:33', '2021-03-07 07:13:57'),
(16, 'patient demo-1', 'father-1', 'good', 'black', 'web-developer', '23', '65', 'Male', 'ranarOffice', '12', '2', '2', '1', '0', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'dvc asdv', 'fv zsdfvszdf', 'fdvbdsf vb', 'adf vbdfv', '2021-03-03 12:56:31', '2021-03-03 12:57:03', '2021-03-06 09:09:33'),
(19, 'patient-1', 'father-1', 'good', 'white', 'web-developer', '23', '65', 'Male', 'ranarOffice', '12', 'dd', 'gfnbfg', '1', '0', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'rvrfvdevbasbtf', 'fdb vdsfgbdfgb', 'dfbvdfbdf', 'dfbdfbfdb', '2021-03-04 04:38:56', '2021-03-04 04:39:25', '2021-03-06 09:09:44'),
(20, 'patient-2', 'father-2', 'bad', 'black', 'Software developer', '23', '65', 'Male', 'ranarOffice', '12', '1200', '124', '5', '0', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'amar family te karo kono somossa nei', 'amar bortoman somossa allergy', 'janina', 'description', '2021-03-04 04:54:23', '2021-03-04 04:54:23', '2021-03-04 05:24:31'),
(22, 'patient2', 'father-2', 'good', 'white', 'web-developer', '23', '65', 'Male', 'address', '11', '123456', '251', '5', '0', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'rgsertgbaesrtgbawergarg', 'asrvgbwarvgbearvgerg', 'edzsvgbdfbadfb', 'srfvgsdvgbasdfg', '2021-03-06 06:37:15', '2021-03-06 06:37:15', '2021-03-06 06:37:15');

-- --------------------------------------------------------

--
-- Table structure for table `prescriptions`
--

CREATE TABLE `prescriptions` (
  `id` int(11) NOT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` varchar(255) NOT NULL,
  `less` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prescriptions`
--

INSERT INTO `prescriptions` (`id`, `patient_id`, `date`, `status`, `less`, `created_at`, `updated_at`) VALUES
(39, 22, '2021-03-08 03:53:51', 'Dispensed', '', '2021-03-08 03:53:51', '2021-03-08 03:54:54'),
(40, 16, '2021-03-08 03:59:52', 'Dispensed', '', '2021-03-08 03:59:52', '2021-03-08 04:08:38'),
(41, 19, '2021-03-08 04:11:30', 'Dispensed', '', '2021-03-08 04:11:30', '2021-03-08 04:12:58'),
(42, 16, '2021-03-08 04:28:07', 'Dispensed', '', '2021-03-08 04:28:07', '2021-03-08 04:29:43'),
(43, 10, '2021-03-08 04:34:41', 'Dispensed', '6', '2021-03-08 04:34:41', '2021-03-08 04:38:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `reset_password_token` varchar(255) NOT NULL,
  `reset_password_expire` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `role`, `designation`, `status`, `reset_password_token`, `reset_password_expire`, `created_at`, `updated_at`) VALUES
(4, 'john doe', 'john@gmail.com', '$2a$10$EEFRYmr1.vIxdqFW7yYBP.fTeKQQijADQnzTSbbvIf42YI9ovOASm', '', 'Operator', '', 'Active', '', '0000-00-00', '2021-02-25 05:08:03', '2021-02-25 05:08:03'),
(5, '', 'john1@gmail.com', '$2a$10$Kg4xMkYEIfsf6Uoco4tWrOgGHcKS3ggBV2sE3KcBcFqJ4zVDnCDDi', '', 'Admin', '', 'Active', '', '0000-00-00', '2021-02-25 05:26:03', '2021-02-25 05:26:03'),
(6, 'Brad Traversy', 'brad@gmail.com', '$2a$10$IBjT8shI8Hwr7EeBZ3FF7uLkOQmwWpK8DJ01.TpbfqS/iU3.qR8te', '', 'User', '', 'Active', '', '0000-00-00', '2021-02-25 09:32:46', '2021-02-25 09:32:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `medicines`
--
ALTER TABLE `medicines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prescriptions`
--
ALTER TABLE `prescriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `medicines`
--
ALTER TABLE `medicines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `prescriptions`
--
ALTER TABLE `prescriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
